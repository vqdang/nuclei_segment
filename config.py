
import operator
import random
import math
import cv2
import tensorflow as tf
import argparse
import numpy as np
import os

from tensorpack import *
from tensorpack.tfutils.symbolic_functions import *
from tensorpack.tfutils.summary import *

import matplotlib.pyplot as plt
from matplotlib import cm

####
from model.net import Model

class Model(Model):
    def __init__(self, desc, steps_per_epoch=None):
        assert tf.test.is_gpu_available()
        self.data_format = 'NCHW'
        self.steps_per_epoch = steps_per_epoch
        self.init_lr    = desc.init_lr
        self.freeze     = desc.model_freeze
        self.model_mode  = desc.model_mode # 'blob' or 'dst_grad'
    def _get_inputs(self):
        return [InputDesc(tf.float32, [None, 270, 270, 3], 'image'),
                InputDesc(tf.float32, [None, 80, 80, 2], 'truemap-coded')]
          
    def _get_optimizer(self):
        lr = get_scalar_var('learning_rate', self.init_lr, summary = True)
        opt = tf.train.AdamOptimizer(lr)
        return opt
#### 
class Config:
    def __init__(self):
        ####
        train_diff_shape = 190
        self.train_base_shape = [0, 0]
        self.train_mask_shape = [80, 80] # 
        self.train_base_shape[0] = self.train_mask_shape[0] + train_diff_shape
        self.train_base_shape[1] = self.train_mask_shape[1] + train_diff_shape
        ####
        valid_diff_shape = 190
        self.valid_base_shape = [0, 0]
        self.valid_mask_shape = [80, 80] 
        self.valid_base_shape[0] = self.valid_mask_shape[0] + valid_diff_shape
        self.valid_base_shape[1] = self.valid_mask_shape[1] + valid_diff_shape

        ####
        self.model_freeze = False
        self.model_mode   = 'dst_grad' # 'blb' or 'dst_grad'
        infer_mode = 'valid'

        save_mode = 'blb' if self.model_mode == 'blb' else 'dst'
        #### Training data
        self.do_valid = True
        
        self.train_dir = ['/mnt/dang/train/KUMAR/paper/train/XXXX/']
        self.valid_dir = ['/mnt/dang/train/KUMAR/paper/valid_same/XXXX/',
                          '/mnt/dang/train/KUMAR/paper/valid_diff/XXXX/']

        self.data_ext = '.npy'
        self.data_shape = [270, 270]
        
        # re-use the ones output by simon's unet
        # path to file which contains mean / variance of target data
        self.stat_path = None

        #### Training parameters
        self.lr_steps   = 30  # decrease at every n-th epoch  
        self.lr_factor  = 10  # steps by factor
        self.init_lr    = 1.0e-4
        self.nr_epochs  = 60
        self.nr_classes = 2
        ####

        self.train_batch_size = 4
        self.valid_batch_size = 16

        ####
        # loading chkpts in tensorflow, the path must not contain extra '/'
        self.log_path = '/mnt/dang/output/NUC-SEG/collab/'
        self.save_dir   = self.log_path + 'v4.0.0.1/%s_XYRA/tune/' % (save_mode)

        if self.model_freeze:
            self.load_mode = 'new'
            self.pretrained_path = '../../pretrained/ImageNet-ResNet50-Preact.npz'
            # self.pretrained_path = '../../pretrained/ImageNet-DenseNet121.npz'
        else:
            self.load_mode = 'tune'
            self.pretrained_path = self.log_path + 'v4.0.0.1/%s_XYRA/model-10140.index' % (save_mode)

        ####

        self.inf_manual_chkpts = False
        self.inf_model_path  = self.save_dir + 'model-38870.index'
        self.inf_eval_metric = 'valid_dice' if save_mode == 'blb' else 'valid_mse'
        self.inf_comparator = '>' if save_mode == 'blb' else '<'
        self.inf_output_dir = 'output/v4.0.0.1_%s/%s/' % (infer_mode, save_mode)

        self.inf_imgs_ext = '.tif'
        self.inf_imgs_dir = '/mnt/dang/data/KUMAR/train-set/imgs_norm/XXXX/'
        self.inf_norm_dir = '/mnt/dang/data/KUMAR/train-set/imgs_norm/'
        self.inf_norm_codes = ['XXXX', '5784']
        
        self.pred_tensors = ['predmap-prob']
        #### to feed tensor output from GPU->CPU
        self.tensor_names = ['predmap', 'truemap']

        return

####
class StatCollector(Inferencer):
    def __init__(self, desc, prefix='valid'):
        self.model_mode = desc.model_mode
        self.tensor_names = desc.tensor_names
        self.prefix = prefix

    def _get_fetches(self):
        return self.tensor_names

    def _before_inference(self):
        self.nr_ele   = 0
        self.nr_corr  = 0
        self.nr_total = 0
        self.nr_inter = 0
        self.total_error = 0

    def _on_fetches(self, outputs):
        pred, true = outputs
        assert pred.shape == true.shape, "{} != {}".format(pred.shape, true.shape)

        self.nr_ele   += (np.ones(true.shape)).sum()
        # classification
        if self.model_mode == 'blb':
            self.nr_corr  += (pred == true).sum()
            self.nr_inter += (pred *  true).sum()
            self.nr_total += (pred +  true).sum()
        else: # regression
            error = pred - true
            self.total_error += (error * error).sum()

    def _after_inference(self):
        #
        if self.model_mode == 'blb':
            accuracy = self.nr_corr / self.nr_ele
            dice = 2 * self.nr_inter / self.nr_total
            return {self.prefix + '_acc' : accuracy,
                    self.prefix + '_dice': dice,}
        else:
            mse = self.total_error / self.nr_ele           
            return {self.prefix + '_mse' : mse,
                    self.prefix + '_err' : self.total_error}