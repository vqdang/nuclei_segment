
import random
import math
import cv2
import argparse
import numpy as np
from six.moves import zip
import os
import sys

from tensorpack import *
from tensorpack.tfutils.symbolic_functions import *
from tensorpack.tfutils.summary import *
from tensorpack.utils.utils import get_rng

import matplotlib.pyplot as plt

from .augs import *

####
class DatasetSerial(RNGDataFlow):
    """
    Produce ``(image, label)`` pair, where 
        ``image`` has shape (H, W, C (3(BGR))) and ranges in [0,255].
        ``Label`` is an int image of shape (H, W, classes) in range [0, no.classes - 1].
        ``dist_sample
    """

    def __init__(self, path_list):
        self.path_list = path_list
    ##
    def size(self):
        return len(self.path_list)
    ##
    def get_data(self):
        idx_list = list(range(0, len(self.path_list)))
        random.shuffle(idx_list)
        for idx in idx_list:

            data = np.load(self.path_list[idx])

            # split stack channel into image and label
            img = data[...,:3]
            ann = data[...,3:]

            blb = ann[...,-1]
            blb[blb > 0] = 1

            img = img.astype('uint8')
            
            yield [img, ann]
###########################################################################
#### 
def valid_generator(ds, data_shape, label_shape, batch_size = 16):

    ### augmentation for both the input and label
    shape_aug = [
        imgaug.CenterCrop(data_shape),
    ]
    ds = AugmentImageComponents(ds, shape_aug, (0, 1), copy=True)

    ### augmentation for just the output
    augmentors = [
        imgaug.CenterCrop(label_shape),
    ]        
    ds = AugmentImageComponent(ds, augmentors, index = 1, copy=True)

    ds = BatchData(ds, batch_size, remainder=True)

    return ds
####
def train_generator(ds, data_shape, label_shape, batch_size=16):

    ### augmentation for both the input and label
    shape_augs = [
        imgaug.Affine(
                    shear=5, # in degree
                    scale=(0.8, 1.2),
                    rotate_max_deg=179,
                    translate_frac=(0.01, 0.01),
                    interp=cv2.INTER_NEAREST,
                    border=cv2.BORDER_CONSTANT),
        imgaug.Flip(vert=True),
        imgaug.Flip(horiz=True),
        imgaug.CenterCrop(data_shape),
    ]
    ds = AugmentImageComponents(ds, shape_augs, (0, 1), copy=True)
    ### augmentation for just the input
    augmentors = [
        imgaug.RandomApplyAug(
            imgaug.RandomChooseAug(
                [
                GaussianBlur(),
                MedianBlur(),
                imgaug.GaussianNoise(),
                ]
            ), 0.5),
        # standard color augmentation
        imgaug.RandomOrderAug(
            [imgaug.Hue((-8, 8), rgb=True), # 0.04 * 90 degree
             imgaug.Saturation(0.2, rgb=True),
             imgaug.Brightness(26, clip=True), # 0.1 * 255 
             imgaug.Contrast((0.75, 1.25), clip=True),
            ]),
        imgaug.ToUint8(),
    ]
    ds = AugmentImageComponent(ds, augmentors, index=0, copy=False)

    ### augmentation for just the output
    augmentors = [
        imgaug.CenterCrop(label_shape),        
    ]
    ds = AugmentImageComponent(ds, augmentors, index=1, copy=True)

    ds = BatchDataByShape(ds, batch_size, idx=0)
    ds = PrefetchDataZMQ(ds, 8)

    return ds

#### 
def view_data_serial(datagen, silent=False):
    counter = 0
    ds = RepeatedData(datagen, -1)    
    ds.reset_state()
    for imgs, segs in ds.get_data():
        for img, seg in zip(imgs, segs):
            if silent:
                print(counter)
                counter += 1
                continue
            plt.subplot(2,2,1)
            plt.imshow(img)
            plt.subplot(2,2,2)
            plt.imshow(seg[...,1])
            plt.subplot(2,2,3)
            plt.imshow(seg[...,-1])
            plt.subplot(2,2,4)
            plt.imshow(seg[...,-2])
            plt.show()
###

###########################################################################
