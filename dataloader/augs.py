
import math

import cv2
import numpy as np
from scipy import ndimage
from scipy.ndimage import measurements
from scipy.ndimage.filters import gaussian_filter
from scipy.ndimage.interpolation import affine_transform, map_coordinates
from scipy.ndimage.morphology import (binary_dilation, distance_transform_cdt,
                                      distance_transform_edt)
from skimage import morphology as morph
from tensorpack import *
from tensorpack.tfutils.summary import *
from tensorpack.tfutils.symbolic_functions import *
from tensorpack.utils.utils import get_rng

from datagen.utils import *


####
class ColorAugment(imgaug.ImageAugmentor):
    def __init__(self, ch_shuffle=False, random_hsv=True):
        self.ch_shuffle = ch_shuffle
        self.random_hsv = random_hsv
        pass
    def reset_state(self):
        self.rng = get_rng(self)
    def _augment(self, img, _):
        # randomiz in HSV and map back RGB
        if self.ch_shuffle and self.rng.uniform(0.0, 1.0) > 0.5:
            hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
            hsv = hsv.astype("float32") # to avoid overflow when mixing HS with noise
            hs = hsv[...,:2]
            hs = hsv[...,:2] * self.rng.uniform(low=0.7, high=1.2, size=hs.shape)
            hs[hs > 255.0] = 255.0 # threshold before turn back to int
            hsv[...,:2] = hs # re-assign and turn back to RGB
            img = cv2.cvtColor(hsv.astype("uint8"), cv2.COLOR_HSV2RGB) 
        # random shuffle color channel
        if self.ch_shuffle and self.rng.uniform(0.0, 1.0) > 0.5:
            chs = np.dsplit(img, 3) # assumme NHWC
            self.rng.shuffle(chs)
            img = np.concatenate(chs, axis=-1)
        return img
####
class BackgroundDistanceMap(imgaug.ImageAugmentor):   
    def reset_state(self):
        self.rng = get_rng(self)
    def _augment(self, img, _):
        dst = img[...,-1]
        bgd = img[...,0]

        bgd_dst = distance_transform_edt(bgd)
        bgd_dst = normalize(bgd_dst)
        img[...,-2] = dst + bgd_dst
        return img
####
class BlobWeight(imgaug.ImageAugmentor):   
    def reset_state(self):
        self.rng = get_rng(self)
    def _augment(self, img, _):
        pen = img[...,2]
        blb = img[...,1]

        blob_label, _ = blob_labelling(blb)
        blob_list = list(np.unique(blob_label))
        blob_list.remove(0)
        for blob_id in blob_list:
            blob_size = (blob_label == blob_id).sum()
            blob_scale = math.exp(-math.log(blob_size / 3100))
            blob_scale = 1 if blob_size >= 3100 else blob_scale
            pen[blob_label == blob_id] = blob_scale
        img[...,2] = pen
        return img
####
class GenInstanceXY(imgaug.ImageAugmentor):   
    """
        Input annotation must be of original shape.
        The map is calculated only for instances within the crop portion
        but basing on the original shape in original image
    """
    def __init__(self, crop_shape=None):
        self.crop_shape = crop_shape
        pass
    def reset_state(self):
        self.rng = get_rng(self)
    def _augment(self, img, _):
        orig_ann = img[...,1] # instance ID map
        crop_ann = cropping_center(orig_ann, self.crop_shape)

        # deal with duplicated instances
        # due to mirroring in interpolation
        id_counter = 0
        current_max_id = np.amax(orig_ann)
        inst_list = list(np.unique(orig_ann))
        inst_list.remove(0) # 0 is background
        for inst_id in inst_list:
            inst_map = np.array(orig_ann == inst_id, np.uint8)
            remap_inst_ids = measurements.label(inst_map)[0]
            remap_inst_ids[remap_inst_ids > 1] += current_max_id
            orig_ann[remap_inst_ids > 1] = remap_inst_ids[remap_inst_ids > 1]
            current_max_id = np.amax(orig_ann)
        # re-cropping with fixed instance id map
        crop_ann = cropping_center(orig_ann, self.crop_shape)
        # removing rear pixels that are too small for balancing com
        crop_ann = morph.remove_small_objects(crop_ann, min_size=40)

        x_map = np.zeros(orig_ann.shape[:2], dtype=np.float32)  
        y_map = np.zeros(orig_ann.shape[:2], dtype=np.float32)  

        inst_list = list(np.unique(crop_ann))
        inst_list.remove(0) # 0 is background
        for inst_id in inst_list:
            inst_map = np.array(orig_ann == inst_id, np.uint8)
            inst_box = bounding_box(inst_map)
            inst_map = inst_map[inst_box[0]:inst_box[1],
                                inst_box[2]:inst_box[3]]

            # instance center of mass, rounded to nearest pixel
            inst_com = list(measurements.center_of_mass(inst_map))
            
            inst_com[0] = int(inst_com[0] + 0.5)
            inst_com[1] = int(inst_com[1] + 0.5)

            inst_x_range = np.arange(1, inst_map.shape[1]+1)
            inst_y_range = np.arange(1, inst_map.shape[0]+1)
            # shifting center of pixel to instance center of mass
            inst_x_range -= inst_com[1]
            inst_y_range -= inst_com[0]
            
            inst_x, inst_y = np.meshgrid(inst_x_range, 
                                         inst_y_range)

            # remove coord outside of instance
            inst_x[inst_map == 0] = 0
            inst_y[inst_map == 0] = 0
            inst_x = inst_x.astype('float32')
            inst_y = inst_y.astype('float32')

            # normalize min into -1 scale
            inst_x[inst_x < 0] /= (-np.amin(inst_x[inst_x < 0]))
            inst_y[inst_y < 0] /= (-np.amin(inst_y[inst_y < 0]))
            # normalize max into +1 scale
            inst_x[inst_x > 0] /= (np.amax(inst_x[inst_x > 0]))
            inst_y[inst_y > 0] /= (np.amax(inst_y[inst_y > 0]))

            ####
            x_map_box = x_map[inst_box[0]:inst_box[1],
                              inst_box[2]:inst_box[3]]
            x_map_box[inst_map > 0] = inst_x[inst_map > 0]

            y_map_box = y_map[inst_box[0]:inst_box[1],
                              inst_box[2]:inst_box[3]]
            y_map_box[inst_map > 0] = inst_y[inst_map > 0]

        #
        img = img.astype('float32')
        img[...,-1] = x_map
        img[...,-2] = y_map

        return img
####
class GenInstanceDistance(imgaug.ImageAugmentor):   
    """
        Input annotation must be of original shape.
        The map is calculated only for instances within the crop portion
        but basing on the original shape in original image
    """
    def __init__(self, crop_shape=None):
        self.crop_shape = crop_shape
        pass
    def reset_state(self):
        self.rng = get_rng(self)
    def _augment(self, img, _):
        orig_dst = img[...,0]
        orig_ann = img[...,1] # instance ID map
        crop_ann = cropping_center(orig_ann, self.crop_shape)

        # deal with duplicated instances
        # due to mirroring in interpolation
        id_counter = 0
        current_max_id = np.amax(orig_ann)
        inst_list = list(np.unique(orig_ann))
        inst_list.remove(0) # 0 is background
        for inst_id in inst_list:
            inst_map = np.array(orig_ann == inst_id, np.uint8)
            remap_inst_ids = measurements.label(inst_map)[0]
            remap_inst_ids[remap_inst_ids > 1] += current_max_id
            orig_ann[remap_inst_ids > 1] = remap_inst_ids[remap_inst_ids > 1]
            current_max_id = np.amax(orig_ann)
        # re-cropping with fixed instance id map
        crop_ann = cropping_center(orig_ann, self.crop_shape)

        orig_dst = np.zeros(orig_dst.shape, dtype=np.float32)  

        inst_list = list(np.unique(crop_ann))
        inst_list.remove(0) # 0 is background
        for inst_id in inst_list:
            inst_map = np.array(orig_ann == inst_id, np.uint8)
            inst_box = bounding_box(inst_map)

            # expand the box by 2px
            inst_box[0] -= 2
            inst_box[2] -= 2
            inst_box[1] += 2
            inst_box[3] += 2

            inst_map = inst_map[inst_box[0]:inst_box[1],
                                inst_box[2]:inst_box[3]]

            if inst_map.shape[0] < 2 or \
                inst_map.shape[1] < 2:
                continue

            inst_dst = distance_transform_edt(inst_map)
            inst_dst = 255 * (inst_dst / np.amax(inst_dst)) 
            inst_dst = inst_dst.astype('uint8')      

            ####
            dst_map_box = orig_dst[inst_box[0]:inst_box[1],
                                   inst_box[2]:inst_box[3]]
            dst_map_box[inst_map > 0] = inst_dst[inst_map > 0]

        #
        img[...,0] = orig_dst
        return img
####
class GaussianBlur(imgaug.ImageAugmentor):
    """ Gaussian blur the image with random window size"""
    def __init__(self, max_size=3):
        """
        Args:
            max_size (int): max possible Gaussian window size would be 2 * max_size + 1
        """
        super(GaussianBlur, self).__init__()
        self._init(locals())
    def _get_augment_params(self, img):
        sx, sy = self.rng.randint(1, self.max_size, size=(2,))
        sx = sx * 2 + 1
        sy = sy * 2 + 1
        return sx, sy
    def _augment(self, img, s):
        return np.reshape(cv2.GaussianBlur(img, s, sigmaX=0, sigmaY=0,
                                           borderType=cv2.BORDER_REPLICATE), img.shape)
#####
class MedianBlur(imgaug.ImageAugmentor):
    def __init__(self, max_size=3):
        """
        Args:
            max_size (int): max possible window size 
                            would be 2 * max_size + 1
        """
        super(MedianBlur, self).__init__()
        self._init(locals())
    
    def _get_augment_params(self, img):
        s = self.rng.randint(1, self.max_size)
        s = s * 2 + 1
        return s
    def _augment(self, img, ksize):
        return cv2.medianBlur(img, ksize)
#### 
class LensDistort(imgaug.ImageAugmentor):
    def __init__(self, k_range=[0.1, 0.2], borderEdge='crop', 
                        interp=cv2.INTER_NEAREST, border=cv2.BORDER_CONSTANT, borderValue=0):
        """
        border: 'reflect', 'constant', 'nearest', 'mirror', 'wrap'
        borderValue: used when border mode is 'constant'
        """
        super(LensDistort, self).__init__()
        self._init(locals())
        pass
    def reset_state(self):
        self.rng = get_rng(self)
    def test_run(self, img):
        return self._augment(img, None)
    def _augment(self, img, _):
        #
        def cart2pol(x, y):
            rho = np.sqrt(x*x + y*y)
            phi = np.arctan2(y, x)
            return phi, rho
        #
        def pol2cart(phi, rho):
            x = rho * np.cos(phi)
            y = rho * np.sin(phi)
            return x, y
        #
        def bordercorrect(r, s, k, center, R, borderEdge):
            if k < 0:
                if borderEdge == 'fit':
                    x = r[0] / s[0] 
                if borderEdge == 'crop':    
                    x = 1/(1 + k*(min(center)/R)**2)
            elif k > 0:
                if borderEdge == 'fit':
                    x = 1/(1 + k*(min(center)/R)**2)
                if borderEdge == 'crop':
                    x = r[0] / s[0]
            return x
        #
        def distortfun(r, k, fcnum):
                if fcnum == 1:
                    s = r * (1 / (1 + k * r))
                elif fcnum == 2:
                    s = r * (1 / (1 + k * r * r))
                elif fcnum == 3:
                    s = r * (1 + k * r)
                elif fcnum == 4:
                    s = r * (1 + k * r * r)
                else:
                    assert False, 'fstyle must be one of 1, 2, 3 or 4'
                return s
        #
        ftype = self.rng.randint(low=1, high=4)
        k = self.rng.uniform(low=self.k_range[0], high=self.k_range[1])
        k = -k if self.rng.uniform() > 0.5 else k
        
        # only deal with HWC
        if img.shape == 2:
            img = np.expand_dims(img, axis=2)

        h = img.shape[0]
        w = img.shape[1]
        c = [round(h/2), round(w/2)]
        # Creates N x M (#pixels) x-y points
        xi, yi = np.meshgrid(range(0, h), range(0, w))
        # Convert the mesh into a colum vector of 
        # coordiantes relative tothe center
        xt = xi - c[0]
        yt = yi - c[1]
        xt = xt.flatten()
        yt = yt.flatten()
        # Converts the x-y coordinates to polar coordinates
        theta, r = cart2pol(xt,yt)
        # Get the maximum vector (image center to image corner)
        #  to be used for normalization
        R = math.sqrt(c[0] * c[0] + c[1] * c[1])
        # Normalize the polar coordinate r to range between 0 and 1 
        r = r / R
        # Aply the r-based transformation
        s = distortfun(r, k, ftype)
        # un-normalize s
        s2 = s * R
        # Find a scaling parameter based on selected border type  
        brcor = bordercorrect(r, s, k, c, R, self.borderEdge)
        
        s2 = s2 * brcor

        # Convert back to cartesian coordinates
        ut, vt = pol2cart(theta,s2)
        
        u = np.reshape(ut, xi.shape) + c[0]
        v = np.reshape(vt, yi.shape) + c[1]
        tmap_B = np.dstack([u, v]).astype('float32')

        output = cv2.remap(img, tmap_B, None, 
                            interpolation=self.interp, 
                            borderMode=self.border, 
                            borderValue=self.borderValue)

        output = np.squeeze(output)
        return output
####
class ElasticDeform(imgaug.ImageAugmentor):
    def __init__(self, alpha=900, sigma=[8.5,10], border='constant',borderValue=0):
        """
        sigma: range of sigma value
        border: 'reflect', 'constant', 'nearest', 'mirror', 'wrap'
        borderValue: used when border mode is 'constant'
        """
        super(ElasticDeform, self).__init__()
        self._init(locals())
        pass
    def reset_state(self):
        self.rng = get_rng(self)
    def test_run(self, img):
        return self._augment(img, None)
    def _augment(self, img, _):
        """Elastic deformation of images as described in [Simard2003]_.
        .. [Simard2003] Simard, Steinkraus and Platt, "Best Practices for
        Convolutional Neural Networks applied to Visual Document Analysis", in
        Proc. of the International Conference on Document Analysis and
        Recognition, 2003.
        """

        shape = img.shape
        sigma_x = self.rng.uniform(low=self.sigma[0], high=self.sigma[1])
        sigma_y = self.rng.uniform(low=self.sigma[0], high=self.sigma[1])

        dx = gaussian_filter((self.rng.rand(*shape) * 2 - 1), sigma_x, 
                                    mode=self.border, cval=self.borderValue) * self.alpha
        dy = gaussian_filter((self.rng.rand(*shape) * 2 - 1), sigma_y, 
                                    mode=self.border, cval=self.borderValue) * self.alpha
        dz = np.zeros_like(dx)

        x, y, z = np.meshgrid(np.arange(shape[0]), np.arange(shape[1]), np.arange(shape[2]))
        indices = np.reshape(y+dy, (-1, 1)), np.reshape(x+dx, (-1, 1)), np.reshape(z, (-1, 1))

        distored_img = map_coordinates(img, indices, order=1, mode='nearest')
        return distored_img.reshape(img.shape)

####
if __name__ == '__main__':
    import matplotlib.pyplot as plt

    # for debugging
    # trans = ElasticDeform()
    trans = LensDistort()
    trans.reset_state()

    img = cv2.imread('sample.png')
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    out = trans.test_run(img)
    out = cv2.cvtColor(out, cv2.COLOR_RGB2BGR)
    cv2.imwrite('out.png', out)
