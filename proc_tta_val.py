
import argparse
import colorsys
import glob
import math
import os
import random
import re  # regex

import cv2
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
from matplotlib import cm
from scipy.ndimage import measurements
from scipy.ndimage.morphology import (binary_dilation, binary_erosion,
                                      binary_fill_holes,
                                      distance_transform_edt)
from skimage import morphology as morph
from skimage.feature import peak_local_max
from skimage.filters import rank, threshold_local, threshold_otsu
from skimage.morphology import disk

from datagen.utils import rm_n_mkdir
from stats_utils import remap_label
from viz_utils import *


#####
def cluster_border(nuc_blob_map, labeled_nuc_map, radius=5):
    nuc_blob_map = np.array(nuc_blob_map, np.int32)
    labeled_nuc_map = np.array(labeled_nuc_map, np.int32)

    labeled_map = np.array(labeled_nuc_map > 0)
    unlabeled_pixels = labeled_map - nuc_blob_map 
    unlabeled_pixels = np.transpose(np.nonzero(unlabeled_pixels))

    nuc_id = np.unique(labeled_nuc_map)
    bgd_id = np.max(nuc_id) + 1

    def gen_slice(center, range, limit):
        slice = [0, 0]
        if center < range:
            slice[0] = center
        else:
            slice[0] = center - range
        if center > limit - range:
            slice[1] = center
        else:
            slice[1] = center + range
        # return relative position of the new center
        new_center = center-slice[0]       
        return new_center, slice

    labeled_cluster_nuc_map = np.copy(labeled_nuc_map)
    for idx in range(0, unlabeled_pixels.shape[0]):
        pos = unlabeled_pixels[idx]
        # get a window surrounding the pixel, 11x11
        npos_y, slice_y = gen_slice(pos[0], radius, labeled_nuc_map.shape[0]-1) 
        npos_x, slice_x = gen_slice(pos[1], radius, labeled_nuc_map.shape[1]-1) 

        window_pixel = labeled_nuc_map[slice_y[0]:slice_y[1]+1,
                                       slice_x[0]:slice_x[1]+1]
        window_pixel = np.copy(window_pixel)
        # make sure the center is background
        # and the default background is nonzero(another id)
        window_pixel[window_pixel == 0] = bgd_id
        window_pixel[npos_y, npos_x] = 0        
        # now calculate nearest distance
        dst_blobs = distance_transform_edt(window_pixel)
        # set all background distances to infinity
        dst_blobs[npos_y, npos_x] = float('Inf')        
        dst_blobs[window_pixel == bgd_id] = float('Inf')
        # get the position of min distance
        pos_min_dst = np.unravel_index(dst_blobs.argmin(), dst_blobs.shape)
        # assign the pixel with that label
        label_posy = pos[0] + pos_min_dst[0] - npos_y 
        label_posx = pos[1] + pos_min_dst[1] - npos_x 
        labeled_cluster_nuc_map[pos[0], pos[1]] = labeled_nuc_map[label_posy, label_posx]

    return labeled_cluster_nuc_map
####
def norm_range(x, scale):
    dtype = x.dtype
    x = x.astype('float32')
    x = (x - np.amin(x)) / (np.amax(x) - np.amin(x))
    return (scale * x).astype(dtype)
####
def marker_level(dst, threshold_level, exclude_level):
    canvas = np.copy(dst)
    canvas[canvas  < threshold_level] = 0
    canvas[canvas >= threshold_level] = 1
    canvas = measurements.label(canvas)[0]
    marker_id_list = list(np.unique(canvas))
    for marker_id in marker_id_list:
        instance_dst = dst[canvas == marker_id]
        if np.amax(instance_dst) >= exclude_level:
            canvas[canvas == marker_id] = 0
    return canvas        
####

random.seed(10) 

threshold_mode = 'hard'

####
valid_same_name = [
    'TCGA-21-5786-01Z-00-DX1',
    'TCGA-49-4488-01Z-00-DX1',
    'TCGA-A7-A13F-01Z-00-DX1',
    'TCGA-B0-5698-01Z-00-DX1',
    'TCGA-B0-5710-01Z-00-DX1',
    'TCGA-CH-5767-01Z-00-DX1',
    'TCGA-E2-A1B5-01Z-00-DX1',
    'TCGA-G9-6336-01Z-00-DX1',]

valid_diff_name = [
    'TCGA-AY-A8YK-01A-01-TS1',
    'TCGA-DK-A2I6-01A-01-TS1',
    'TCGA-G2-A2EK-01A-02-TSB',
    'TCGA-KB-A93J-01A-01-TS1',
    'TCGA-NH-A8F7-01A-01-TS1',
    'TCGA-RD-A8N9-01A-01-TS1',]

blb_dir = 'output/v4.0.0.1_valid/blb/5784/'
dst_dir = 'output/v4.0.0.1_valid/dst/XXXX/'
out_dir = 'output/v4.0.0.1_valid/proc/'
imgs_dir = '/mnt/dang/data/KUMAR/train-set/imgs_norm/XXXX/'
true_dir = '/mnt/dang/data/KUMAR/train-set/anns_proc/imgs_all/' # debug valid

# 'XXXX', '5784','A1AS','A2I6','A13E'
targets_list = [''] 

file_list = glob.glob(imgs_dir + '*.tif')
file_list.sort() # ensure same order [1]

# rm_n_mkdir(out_dir)
for filename in file_list[:]: # png for base
    filename = os.path.basename(filename)
    basename = filename.split('.')[0]
    # print(basename)

    if basename not in valid_diff_name:
        continue

    img = cv2.imread(imgs_dir + basename + '.tif')
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    blb_list = []
    dst_list = []
    for target_code in targets_list:
        blb = sio.loadmat('%s/%s/%s.mat' % (blb_dir, target_code, basename))['result']
        dst = sio.loadmat('%s/%s/%s.mat' % (dst_dir, target_code, basename))['result']
        # avg - avg is more stable
        blb = np.amax(np.array(blb), axis=0)
        dst = np.mean(np.array(dst), axis=0)           
        blb_list.append(blb)
        dst_list.append(dst)

    # avg - avg is more stable
    blb = np.mean(np.array(blb_list), axis=0)
    dst = np.amax(np.array(dst_list), axis=0)           

    #------------------------------------------
    pred_1 = np.copy(blb)
    pred_1[pred_1 <  0.5] = 0 # valid 0.5
    pred_1[pred_1 >= 0.5] = 1 # valid 0.5
    pred_1 = binary_fill_holes(pred_1)
    pred_1 = pred_1.astype('uint8')
    pred_1 = measurements.label(pred_1)[0]

    marker = np.zeros(dst.shape, dtype=np.int32)
    marker = marker  + marker_level(dst,  50,  80)
    marker = marker  + marker_level(dst,  80, 125)
    marker = marker  + marker_level(dst, 120, 130)
    canvas = np.copy(dst)
    canvas[canvas  < 130] = 0 # self 125
    canvas[canvas >= 130] = 1 # self 125
    canvas = measurements.label(canvas)[0]
    marker = (marker + canvas) 

    # remove markers close to main marker (1px apart)
    marker = binary_dilation(marker > 0)
    marker = binary_erosion(marker > 0)
    marker = morph.remove_small_objects(marker, min_size=4)
    marker = measurements.label(marker)[0]
    pred_2 = morph.watershed(-dst, 
                            marker, 
                            mask=pred_1 > 0)
    pred = np.copy(pred_2)

    # clean up false positive due to watershed seed
    pred = np.array(pred_1 > 0, np.int32) * pred # NOTE: becareful
    # get artifact pixels into nearest labeled blob
    pred = cluster_border(pred_1 > 0, pred)
    # get the missing blob labeled and put into final results
    miss_blb = np.array(pred_1 > 0, np.int32) - np.array(pred > 0, np.int32)
    miss_blb = measurements.label(miss_blb)[0]
    # remove all faraway dust, these are not missing blobs
    # miss_blb = morph.remove_small_objects(miss_blb, min_size=40)
    # increment the id counter for missing blob
    miss_blb[miss_blb > 0] += np.amax(pred)
    pred += miss_blb # put into main result
    # pred = morph.remove_small_objects(pred, min_size=40)
    pred = remap_label(pred)

    overlay_pred = visualize_instances(pred, img)
    overlay_pred = cv2.cvtColor(overlay_pred, cv2.COLOR_BGR2RGB)
    cv2.imwrite(out_dir + basename + '.png', overlay_pred)
    sio.savemat('%s/%s_predicted_map.mat' % (out_dir, basename), 
                                    {'predicted_map': pred})
