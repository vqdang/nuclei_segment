
import argparse
import colorsys
import glob
import math
import os
import random
import re  # regex

import cv2
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
from matplotlib import cm
from scipy.ndimage import measurements
from scipy.ndimage.morphology import binary_dilation, distance_transform_edt, binary_erosion
from skimage import morphology as morph
from skimage.feature import peak_local_max

from datagen.utils import randomize_label, rm_n_mkdir
from viz_utils import *

#####
def get_aji_v2(true, pred, global_inter, global_union):
    """
    Fast AJI
    """
    true = np.copy(true)
    pred = np.copy(pred)
    true_id = list(np.unique(true))
    pred_id = list(np.unique(pred)) 

    true_masks = [np.zeros(true.shape)]
    for t in true_id[1:]:
        t_mask = np.array(true == t, np.uint8)
        true_masks.append(t_mask)
    
    pred_masks = [np.zeros(true.shape)]
    for p in pred_id[1:]:
        p_mask = np.array(pred == p, np.uint8)
        pred_masks.append(p_mask)
    
    overall_inter = 0
    overall_union = 0
    avail_pred_idx = list(range(1, len(pred_id)))
    for true_idx in range(1, len(true_id)):
        max_iou   = 0
        max_inter = 0 
        max_union = 0
        max_iou_idx = -1

        t_mask = true_masks[true_idx]
        pred_true_overlap = pred[t_mask > 0]
        pred_true_overlap_id = np.unique(pred_true_overlap)
        pred_true_overlap_id = list(pred_true_overlap_id)

        overlap_idx = []
        for idx in pred_true_overlap_id:
            if idx in avail_pred_idx:
                overlap_idx.append(idx)

        for pred_idx in overlap_idx:
            p_mask = pred_masks[pred_idx]
            total = (t_mask + p_mask).sum()
            inter = (t_mask * p_mask).sum()
            union = total - inter
            iou = inter / union
            if iou > max_iou:
                max_iou = iou
                max_inter = inter
                max_union = union
                max_iou_idx = pred_idx

        overall_inter += max_inter
        if max_iou > 0:
            overall_union += max_union
            avail_pred_idx.remove(max_iou_idx)
        else: # total missed, so just add GT
            overall_union += t_mask.sum()            

    # deal with remaining i.e over segmented
    for pred_idx in avail_pred_idx:
        p_mask = pred_masks[pred_idx]
        overall_union += p_mask.sum()
    #
    global_inter += overall_inter
    global_union += overall_union
    aji_score = overall_inter / overall_union
    return aji_score, global_inter, global_union
#####
def get_dice_1(true, pred):
    """
        Traditional dice
    """
    # cast to binary 1st
    true = np.copy(true)
    pred = np.copy(pred)
    true[true > 0] = 1
    pred[pred > 0] = 1
    inter = true * pred
    denom = true + pred
    return 2.0 * np.sum(inter) / np.sum(denom)
#####
def get_dice_2(true, pred):
    """
        Ensemble dice
    """
    true = np.copy(true)
    pred = np.copy(pred)
    true_id = list(np.unique(true))
    pred_id = list(np.unique(pred))
    # remove background aka id 0
    true_id.remove(0)
    pred_id.remove(0)

    total_markup = 0
    total_intersect = 0

    true_masks = []
    true_sums  = []
    for t in true_id:
        t_mask = np.array(true == t)
        true_masks.append(t_mask)
        true_sums.append(t_mask.sum())
    
    pred_masks = []
    pred_sums  = []
    for p in pred_id:
        p_mask = np.array(pred == p)
        pred_masks.append(p_mask)
        pred_sums.append(p_mask.sum())

    for i, t_mask in enumerate(true_masks):
        pred_overlap = pred[t_mask]
        overlap_id = list(np.unique(pred_overlap))
        try: # blinly remove background
            overlap_id.remove(0)
        except ValueError:
            pass  # just mean no background
        for j in overlap_id:
            p_mask = pred_masks[j-1]
            intersect = p_mask * t_mask          
            total_intersect += intersect.sum()
            total_markup += true_sums[i] + pred_sums[j-1]
    return 2 * total_intersect / total_markup
#####
def remap_label(pred, by_size=True):
    """
    Rename all instance id so that the id (sorting from min to max)
    is contiguos i.e [0, 1, 2, 3] not [0, 2, 4, 6].

    Args:
        pred    : the 2d array contain instances where each instances is marked
                  by non-zero integer
        by_size : renaming with larger nuclei has smaller id (on-top)
    """
    pred_id = list(np.unique(pred))
    pred_id.remove(0)
    if by_size:
        pred_size = []
        for inst_id in pred_id:
            size = (pred == inst_id).sum()
            pred_size.append(size)
        # sort the id by size in descending order
        pair_list = zip(pred_id, pred_size)
        pair_list = sorted(pair_list, key=lambda x: x[1], reverse=True)
        pred_id, pred_size = zip(*pair_list)

    new_pred = np.zeros(pred.shape, np.int32)
    for idx, id in enumerate(pred_id):
        new_pred[pred == id] = idx + 1    
    return new_pred
#####
