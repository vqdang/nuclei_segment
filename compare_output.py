
import os
import re 
import cv2
import glob
import math
import numpy as np
from scipy import io as sio
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.backends.backend_pdf import PdfPages

from viz_utils import *
from datagen.utils import rm_n_mkdir

cmap = plt.get_cmap('jet')

fold_list = ['set_one']

for fold_mode in fold_list:

    imgs_dir = '../../data/NUC_Kumar/cross-valid/%s/valid/Images/' % fold_mode
    true_dir = '../../data/NUC_Kumar/train-set/msks_fixed/imgs_all/'
    method1_dir = 'output/mrcnn/val/%s/' % fold_mode
    method2_dir = 'output/v3.0.0.3x_valid/fin_%s/' % fold_mode
    combine_dir = 'output/v3.0.0.3x_valid/fin_%s_x/' % fold_mode

    file_list = glob.glob(imgs_dir + '*.tif')
    file_list.sort() # ensure same order [1]
    file_list.reverse()

    for filename in file_list[1:]: # png for base
        filename = os.path.basename(filename)
        basename = filename.split('.')[0]
        print(basename)

        img = cv2.imread(imgs_dir + basename + '.tif')
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        true = sio.loadmat(true_dir + basename + '.mat')['result']
        overlay_true = visualize_instances(true, img)

        overlay_pred1 = cv2.imread(method1_dir + basename + '.png')
        overlay_pred1 = cv2.cvtColor(overlay_pred1, cv2.COLOR_BGR2RGB)

        overlay_pred2 = cv2.imread(method2_dir + basename + '.png')
        overlay_pred2 = cv2.cvtColor(overlay_pred2, cv2.COLOR_BGR2RGB)

        overlay_predx = cv2.imread(combine_dir + basename + '.png')
        overlay_predx = cv2.cvtColor(overlay_predx, cv2.COLOR_BGR2RGB)

        imgs_list = [img          [625:800, 325:500],
                     overlay_true [625:800, 325:500], 
                     overlay_pred1[625:800, 325:500], 
                     overlay_pred2[625:800, 325:500], 
                     overlay_predx[625:800, 325:500]]
        # imgs_list = [img, overlay_true, overlay_pred1, overlay_pred2, overlay_predx]
        imgs_title = ['Input', 'Ground Truth', 'Method 1', 'Method 2', 'Combine']
        comparison_figure = gen_figure(imgs_list, imgs_title, (8, 8), shape=(1, 5), share_ax='none')
        plt.show()