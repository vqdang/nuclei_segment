
import random
import math
import cv2
import tensorflow as tf
import argparse
import numpy as np
from six.moves import zip
import os
import sys

import tensorflow as tf

from tensorpack import *
from tensorpack.tfutils.symbolic_functions import *
from tensorpack.tfutils.summary import *

from functools import reduce
import scipy.io as sio

from .layers import *
from .util_ops import *

# import model.memory_saving_gradients as memory_saving_gradients
# # monkey patch tf.gradients to point to our custom version, with automatic checkpoint selection
# tf.__dict__["gradients"] = memory_saving_gradients.gradients_memory

class Model(ModelDesc):
    """
        Just bare graph
    """
    def _build_graph(self, inputs):
        self.network(inputs)
        return

    def network(self, inputs, return_graph=False):
        ####
        def upsample2x(name, x):
            return FixedUnPooling(
                        name, x, 2, unpool_mat=np.ones((2, 2), dtype='float32'),
                        data_format='channels_first')
        ####
        def res_blk(name, l, ch, ksize, count, split=1, stride=1, se=False, se_stride=1):
            ch_in = l.get_shape().as_list()
            with tf.variable_scope(name):
                for i in range(0, count):
                    with tf.variable_scope('block' + str(i)):  
                        x = l if i == 0 else BNReLU('preact', l)
                        x = Conv2D('conv1', x, ch[0], ksize[0], nl=BNReLU)
                        x = Conv2D('conv2', x, ch[1], ksize[1], split=split, 
                                        stride=stride if i == 0 else 1, nl=BNReLU)
                        x = Conv2D('conv3', x, ch[2], ksize[2], nl=tf.identity)
                        if (stride != 1 or ch_in[1] != ch[2]) and i == 0:
                            l = Conv2D('convshortcut', l, ch[2], 1, stride=stride)
                        x = tf.stop_gradient(x) if self.freeze else x
                        l = l + x
                        if se:
                            x = non_local_blk('atten', l, se_stride)
                            l = l + x
                # end of each group need an extra activation
                l = BNReLU('bnlast', l)  
            return l      
        ####
        def densec(name, l, ch, ksize, count, split=1, dilate=(1, 1), padding='valid'):
            with tf.variable_scope(name):
                for i in range(0, count):
                    with tf.variable_scope('blk/' + str(i)):
                        x = BNReLU('preact_bna', l)
                        x = Conv2D('conv1', x, ch[0], ksize[0], padding=padding, nl=BNReLU)
                        x = Conv2D('conv2', x, ch[1], ksize[1],
                                        dilation_rate=dilate, padding=padding, split=split)
                        ##
                        if padding == 'valid':
                            x_shape = x.get_shape().as_list()
                            l_shape = l.get_shape().as_list()
                            l = crop_op(l, (l_shape[2] - x_shape[2], 
                                            l_shape[3] - x_shape[3]))

                        l = tf.concat([l, x], axis=1)
                l = BNReLU('blk_bna', l)
            return l
        ####
        
        images, truemap_coded = inputs

        orig_imgs = images

        if self.model_mode != 'blb':
            o_true = truemap_coded[...,:1]
            o_true = tf.identity(o_true, name='truemap')
        else:
            o_true = truemap_coded[...,-1]
            o_true = tf.cast(o_true, tf.int32)
            o_true = tf.identity(o_true, name='truemap')
            o_one  = tf.one_hot(o_true, 2, axis=-1)
            o_true = tf.expand_dims(o_true, axis=-1)

        ####
        def encoder(i):
            d1 = Conv2D('conv',  i, 64, 7, padding='valid', stride=1, nl=BNReLU)
            d1 = res_blk('group0', d1, [ 64,  64,  256], [1, 3, 1], 3, stride=1)                       
            
            d2 = res_blk('group1', d1, [128, 128,  512], [1, 3, 1], 4, stride=2)
            d2 = tf.stop_gradient(d2) if self.freeze else d2

            d3 = res_blk('group2', d2, [256, 256, 1024], [1, 3, 1], 6, stride=2)
            d3 = tf.stop_gradient(d3) if self.freeze else d3

            d4 = res_blk('group3', d3, [512, 512, 2048], [1, 3, 1], 3, stride=2)
            d4 = tf.stop_gradient(d4) if self.freeze else d4
            
            d4 = Conv2D('conv_bot',  d4, 1024, 1, padding='same')
            return [d1, d2, d3, d4]
        ####
        def decoder(i):
            ####
            with tf.variable_scope('u3'):
                u3 = upsample2x('rz', i[-1])
                u3 = tf.add_n([u3, i[-2]])

                u3 = Conv2D('conva', u3, 256, 5, stride=1, padding='valid')   
                u3 = densec('dense', u3, [128, 32], [1, 5], 8, split=4, padding='valid')
                u3 = Conv2D('convf', u3, 512, 1, stride=1)   
            ####
            with tf.variable_scope('u2'):          
                u2 = upsample2x('rz', u3)
                u2 = tf.add_n([u2, i[-3]])

                u2 = Conv2D('conva', u2, 128, 5, stride=1, padding='valid')
                u2 = densec('dense', u2, [128, 32], [1, 5], 4, split=4, padding='valid')
                u2 = Conv2D('convf', u2, 256, 1, stride=1)   
            ####
            with tf.variable_scope('u1'):          
                u1 = upsample2x('rz', u2)
                u1 = tf.add_n([u1, i[-4]])
            return u1
        ####
        with argscope(Conv2D, nl=tf.identity, use_bias=False, # K.he initializer
                      W_init=tf.variance_scaling_initializer(scale=2.0, mode='fan_out')), \
                argscope([Conv2D, MaxPooling, AvgPooling, BatchNorm], data_format=self.data_format):

            i = tf.transpose(images, [0, 3, 1, 2])
            i = stack_coordinate(i, nxy=True, ra=True)

            ####
            d = encoder(i)
            d[0] = crop_op(d[0], (184, 184))
            d[1] = crop_op(d[1], (72, 72))

            ####
            o = decoder(d)
            o = BNReLU('preact_out', o)

            if self.model_mode == 'blb':
                o_logi = Conv2D('conv_out', o, 2, 1, use_bias=True, nl=tf.identity)
                o_logi = tf.transpose(o_logi, [0, 2, 3, 1])
                o_soft = tf.nn.softmax(o_logi, axis=-1)
                o_prob = tf.identity(o_soft[...,1], name='predmap-prob')
                o_prob = tf.expand_dims(o_prob, axis=-1)
                o_pred = tf.argmax(o_soft, axis=-1, name='predmap')
                o_pred = tf.cast(o_pred, tf.int32)
            else:
                o_logi = Conv2D('conv_out', o, 1, 1, use_bias=True, nl=tf.identity)
                o_logi = tf.transpose(o_logi, [0, 2, 3, 1])
                o_pred = o_logi
                o_prob = tf.identity(o_pred, name='predmap-prob')
                o_prob = tf.identity(o_prob, name='predmap')
        ####

        if return_graph:
            return o
        ####
        def get_gradient(l):
            mx= tf.constant([[1, 1, 0, -1, -1]], dtype=tf.float32)
            my= tf.constant([[1], [1], [0], [-1], [-1]], dtype=tf.float32)
            mx = tf.reshape(mx, [1, 5, 1, 1])
            my = tf.reshape(my, [5, 1, 1, 1])
            # central difference to get gradient, ignore the boundary problem    
            dx = 0.5 * tf.nn.conv2d(l, mx, strides=[1, 1, 1, 1], padding='SAME')
            dy = 0.5 * tf.nn.conv2d(l, my, strides=[1, 1, 1, 1], padding='SAME')
            return dx, dy
        ####
        def get_unit_vector(gradient):
            dx, dy = gradient # unpack
            mag = tf.sqrt(dx*dx + dy*dy)
            ndx = dx / (mag + 1.0e-4)
            ndy = dy / (mag + 1.0e-4)
            return ndx, ndy
        ####
        if get_current_tower_context().is_training:
            ######## LOSS
            if self.model_mode == 'blb':
                loss = categorical_crossentropy(o_soft, o_one)
                loss = tf.reduce_mean(loss, name='loss-1')       
                self.cost = loss
                add_moving_summary(self.cost)
            else:
                loss_1 = o_pred - o_true
                loss_1 = loss_1 * loss_1
                loss_1 = tf.reduce_sum(loss_1, name='loss-1')
                if self.model_mode != 'dst_grad':
                    self.cost = loss_1
                    add_moving_summary(self.cost)
                else:
                    #### NOTE: unstable due to defined range of arc-cos
                    # derivation in place
                    pred_grad = get_gradient(o_pred)
                    true_grad = get_gradient(o_true)
                    # turn into unit vector
                    pred_unit = get_unit_vector(pred_grad)
                    true_unit = get_unit_vector(true_grad)
                    pred_unit = tf.concat(pred_unit, axis=-1)
                    true_unit = tf.concat(true_unit, axis=-1)

                    nuclei = tf.cast(truemap_coded[...,-1] > 0, tf.float32)
                    denom = tf.sqrt(tf.reduce_sum(pred_unit * pred_unit, axis=-1))
                    loss_2 = tf.reduce_sum(pred_unit * true_unit, axis=-1)
                    loss_2 = tf.acos((loss_2 * nuclei) / (denom + 1.0e-6))
                    loss_2 = tf.squeeze(loss_2)
                    loss_2 = loss_2 * loss_2 * nuclei 
                    loss_2 = tf.reduce_sum(loss_2, name='loss-2')
                    self.cost = tf.add_n([loss_1, loss_2], name='loss')           
                    add_moving_summary(self.cost, loss_1, loss_2)
            ####

            add_param_summary(('.*/W', ['histogram']))   # monitor W

            #### logging visual sthg
            orig_imgs = tf.cast(orig_imgs  , tf.uint8)
            tf.summary.image('input', orig_imgs, max_outputs=1)

            orig_imgs = crop_op(orig_imgs, (190, 190), "NHWC")
            o_pred = colorize(o_prob[...,0], cmap='jet')
            o_true = colorize(o_true[...,0], cmap='jet')
            o_pred = tf.cast(o_pred * 255, tf.uint8)
            o_true = tf.cast(o_true * 255, tf.uint8)

            if self.model_mode != 'dst_grad':
                viz = tf.concat([orig_imgs, o_pred, o_true], 2)
            else:
                pred_dx = colorize(pred_unit[...,0], vmin=0, vmax=1, cmap='jet')
                pred_dy = colorize(pred_unit[...,1], vmin=0, vmax=1, cmap='jet')
                true_dx = colorize(true_unit[...,0], vmin=0, vmax=1, cmap='jet')
                true_dy = colorize(true_unit[...,1], vmin=0, vmax=1, cmap='jet')
                pred_dx = tf.cast(pred_dx * 255, tf.uint8)
                pred_dy = tf.cast(pred_dy * 255, tf.uint8)
                true_dx = tf.cast(true_dx * 255, tf.uint8)
                true_dy = tf.cast(true_dy * 255, tf.uint8)
                viz = tf.concat([orig_imgs, o_pred, pred_dx, pred_dy,
                                            o_true, true_dx, true_dy], 2)

            tf.summary.image('output', viz, max_outputs=1)

        return
