
import math
import tensorflow as tf
import numpy as np

import tensorflow as tf
from tensorflow.contrib.layers import variance_scaling_initializer

from tensorpack import *
from tensorpack.tfutils.symbolic_functions import *
from tensorpack.tfutils.summary import *

from matplotlib import cm

####
def stack_coordinate(i, nxy=False, ra=False):
    """
    Obtain the static value of Cartesian and Polar Coordinate for
    the given input batch NCHW of images then append it to existing
    channel

    Args:
        i: tensor of shape NCHW, RGB format
    Return:
        tensor of shape NCHW, RGB+XY(Cartesian Coord)+RA(Polar Coord)
    """
    chs_list = [i]
    shape = tf.shape(i)
    batch = shape[0]
    pi = tf.constant(math.pi)

    x_range = tf.range(1, shape[2]+1)
    y_range = tf.range(1, shape[3]+1)
    x, y = tf.meshgrid(x_range, y_range)

    x = tf.expand_dims(x, axis=0) # depth
    x = tf.expand_dims(x, axis=0) # batch
    x = tf.tile(x, [batch, 1, 1, 1])

    y = tf.expand_dims(y, axis=0) # depth
    y = tf.expand_dims(y, axis=0) # batch
    y = tf.tile(y, [batch, 1, 1, 1])

    x = tf.cast(x, tf.float32)
    y = tf.cast(y, tf.float32)

    if ra or nxy:
        # shift xy to center of the image
        nx =  x - tf.cast(shape[2], tf.float32) / 2.0 + 0.5
        ny = -y + tf.cast(shape[3], tf.float32) / 2.0 + 0.5

    if not nxy:
        chs_list.extend([x, y])
    else:
        chs_list.extend([nx, ny])

    if ra:
        r = tf.sqrt(nx * nx + ny * ny)
        a = tf.atan(tf.abs(ny / nx))
        # dicontinuities at 360, so going from 0-359 actually
        a = tf.where(tf.logical_and(nx >  0, ny >  0),        a, a) # q1
        a = tf.where(tf.logical_and(nx <= 0, ny >  0), pi   - a, a) # q2
        a = tf.where(tf.logical_and(nx <= 0, ny <= 0), pi   + a, a) # q3
        a = tf.where(tf.logical_and(nx >  0, ny <  0), 2*pi - a, a) # q4
        a = tf.where(tf.is_nan(a), tf.zeros_like(a), a) # fix nan at center of circle
        # from radian to degree
        a = a * 360 / 2 / pi
        chs_list.extend([r, a])
    
    o = tf.concat(chs_list, axis=1)
    return o
####
def categorical_crossentropy(output, target):
    """
        categorical cross-entropy, accept probabilities not logit
    """
    # scale preds so that the class probas of each sample sum to 1
    output /= tf.reduce_sum(output,
                            reduction_indices=len(output.get_shape()) - 1,
                            keepdims=True)
    # manual computation of crossentropy
    epsilon = tf.convert_to_tensor(10e-8, output.dtype.base_dtype)
    output = tf.clip_by_value(output, epsilon, 1. - epsilon)
    return - tf.reduce_sum(target * tf.log(output),
                            reduction_indices=len(output.get_shape()) - 1)
####
def colorize(value, vmin=None, vmax=None, cmap=None):
    """
    Arguments:
      - value: input tensor, NHWC
      - vmin: the minimum value of the range used for normalization.
        (Default: value minimum)
      - vmax: the maximum value of the range used for normalization.
        (Default: value maximum)
      - cmap: a valid cmap named for use with matplotlib's `get_cmap`.
        (Default: 'gray')
    Example usage:
    ```
    output = tf.random_uniform(shape=[256, 256, 1])
    output_color = colorize(output, vmin=0.0, vmax=1.0, cmap='viridis')
    tf.summary.image('output', output_color)
    ```
    
    Returns a 3D tensor of shape [height, width, 3].
    """

    # normalize
    if vmin is None:
        vmin = tf.reduce_min(value, axis=[1,2])
        vmin = tf.reshape(vmin, [-1, 1, 1])
    if vmax is None:
        vmax = tf.reduce_max(value, axis=[1,2])
        vmax = tf.reshape(vmax, [-1, 1, 1])
    value = (value - vmin) / (vmax - vmin) # vmin..vmax

    # squeeze last dim if it exists
    # NOTE: will throw error if use get_shape()
    # value = tf.squeeze(value)

    # quantize
    value = tf.round(value * 255)
    indices = tf.cast(value, np.int32)

    # gather
    colormap = cm.get_cmap(cmap if cmap is not None else 'gray')
    colors = colormap(np.arange(256))[:, :3]
    colors = tf.constant(colors, dtype=tf.float32)
    value = tf.gather(colors, indices)

    return value
####
def make_image(x, cy, cx, scale_y, scale_x):
    """
    Take 1st image from x and turn channels representations
    into 2D image, with cx number of channels in x-axis and
    cy number of channels in y-axis
    """
    # norm x for better visual
    x = tf.transpose(x,(0,2,3,1)) # NHWC
    max_x = tf.reduce_max(x, axis=-1, keep_dims=True)
    min_x = tf.reduce_min(x, axis=-1, keep_dims=True)
    x = 255 * (x - min_x) / (max_x - min_x)
    ###
    x_shape = tf.shape(x)
    channels = x_shape[-1]
    iy , ix = x_shape[1], x_shape[2] 
    ###
    x = tf.slice(x,(0,0,0,0),(1,-1,-1,-1))
    x = tf.reshape(x,(iy,ix,channels))
    ix += 4
    iy += 4
    x = tf.image.resize_image_with_crop_or_pad(x, iy, ix)
    x = tf.reshape(x,(iy,ix,cy,cx)) 
    x = tf.transpose(x,(2,0,3,1)) #cy,iy,cx,ix
    x = tf.reshape(x,(1,cy*iy,cx*ix,1))
    x = resize_op(x, scale_y, scale_x)
    return tf.cast(x, tf.uint8)
####
def dice_accuracy_stat(pred, true, cid, name):
    true_pixels = tf.cast(tf.equal(true, cid), tf.float32)
    pred_pixels = tf.cast(tf.equal(pred, cid), tf.float32)
    inter = tf.cast(tf.reduce_sum(true_pixels * pred_pixels), tf.float32)
    denom = tf.cast(tf.reduce_sum(true_pixels + pred_pixels), tf.float32)
    dice = tf.clip_by_value(2.0 * inter / denom, 10e-8, 1.0, name=name)
    add_moving_summary(dice)
    return
####
def resize_op(x, height_factor, width_factor, interp='bicubic', data_format='NHWC'):
    if data_format == 'NCHW':
        original_shape = x.get_shape().as_list()
        new_shape = tf.cast(tf.shape(x)[2:], tf.float32)    
        new_shape *= tf.constant(np.array([height_factor, width_factor]).astype('float32'))
        new_shape = tf.cast(new_shape, tf.int32)    
        x = tf.transpose(x, [0, 2, 3, 1])
        if interp == 'bicubic':
            x = tf.image.resize_bicubic(x, new_shape)
        elif interp == 'bilinear':
            x = tf.image.resize_bilinear(x, new_shape)
        else:
            x = tf.image.resize_nearest_neighbor(x, new_shape)
        x = tf.transpose(x, [0, 3, 1, 2])
        x.set_shape((None,
                    original_shape[1] if original_shape[3] is not None else None,
                    int(original_shape[2] * height_factor) if original_shape[2] is not None else None,
                    int(original_shape[3] * width_factor) if original_shape[3] is not None else None))
    else:
        original_shape = x.get_shape().as_list()
        new_shape = tf.cast(tf.shape(x)[1:3], tf.float32)    
        new_shape *= tf.constant(np.array([height_factor, width_factor]).astype('float32'))
        new_shape = tf.cast(new_shape, tf.int32)    
        if interp == 'bicubic':
            x = tf.image.resize_bicubic(x, new_shape)
        elif interp == 'bilinear':
            x = tf.image.resize_bilinear(x, new_shape)
        else:
            x = tf.image.resize_nearest_neighbor(x, new_shape)
        x.set_shape((None,
                    original_shape[1] * height_factor if original_shape[1] is not None else None,
                    original_shape[2] * width_factor if original_shape[2] is not None else None,
                    original_shape[3] if original_shape[3] is not None else None))
    return x  
####
def crop_op(x, cropping, data_format='NCHW'):
    """
    Center crop image
    Args:
        cropping is the substracted portion
    """
    original_shape = x.get_shape().as_list()
    crop_t = cropping[0] // 2
    crop_b = cropping[0] - crop_t
    crop_l = cropping[1] // 2
    crop_r = cropping[1] - crop_l
    if data_format == 'NCHW':
        x = x[:,:,crop_t:-crop_b,crop_l:-crop_r]
    else:
        x = x[:,crop_t:-crop_b,crop_l:-crop_r]
    return x       
####
def pad2d_op(x, padding=((1, 1), (1, 1)), mode='CONSTANT', data_format='NCHW'):
    """Pads the 2nd and 3rd dimensions of a 4D tensor.
    # Arguments
        x: Tensor or variable.
        padding: Tuple of 2 tuples, padding pattern.
        mode: "CONSTANT", "REFLECT" or "SYMMETRIC"
    # Returns
        A padded 4D tensor.
    """
    assert len(padding) == 2
    assert len(padding[0]) == 2
    assert len(padding[1]) == 2

    if data_format == 'NCHW':
        pattern = [[0, 0],
                   [0, 0],
                   list(padding[0]),
                   list(padding[1])]
    else:
        pattern = [[0, 0],
                   list(padding[0]), list(padding[1]),
                   [0, 0]]
    return tf.pad(x, pattern)
####
