import tensorflow as tf
from tensorflow.contrib.layers import variance_scaling_initializer

from tensorpack import *
from tensorpack.tfutils.symbolic_functions import *
from tensorpack.tfutils.summary import *

####
def GroupNorm(x, group):
    """
    # by tensorpack
    https://arxiv.org/abs/1803.08494
    """
    shape = x.get_shape().as_list()
    ndims = len(shape)
    assert ndims in [2, 4]
    chan = shape[1]
    assert chan % group == 0, chan
    group_size = chan // group

    orig_shape = tf.shape(x)
    h, w = orig_shape[2], orig_shape[3]

    x = tf.reshape(x, tf.stack([-1, group, group_size, h, w]))

    mean, var = tf.nn.moments(x, [2, 3, 4], keep_dims=True)

    new_shape = [1, group, group_size, 1, 1]

    beta = tf.get_variable('beta', [chan], initializer=tf.constant_initializer())
    beta = tf.reshape(beta, new_shape)

    gamma = tf.get_variable('gamma', [chan], initializer=tf.constant_initializer(1.0))
    gamma = tf.reshape(gamma, new_shape)

    out = tf.nn.batch_normalization(x, mean, var, beta, gamma, 1e-5, name='output')
    return tf.reshape(out, orig_shape, name='output')

####
def GNReLU(name, x):
    with tf.variable_scope(name + '_gn'):
        x = GroupNorm(x, 4)
        x = tf.nn.relu(x, name=name + '_relu')
    return x
####
@layer_register(log_shape=True)
def UpSample(x, shape):
    """
    Modded the provided BilinearUpSample in tensorpack to use NCHW
    """
    inp_shape = x.shape.as_list()
    ch = inp_shape[1]
    assert ch is not None

    shape = int(shape)
    filter_shape = 2 * shape

    def bilinear_conv_filler(s):
        """
        s: width, height of the conv filter
        See https://github.com/BVLC/caffe/blob/master/include%2Fcaffe%2Ffiller.hpp#L244
        """
        f = np.ceil(float(s) / 2)
        c = float(2 * f - 1 - f % 2) / (2 * f)
        ret = np.zeros((s, s), dtype='float32')
        for x in range(s):
            for y in range(s):
                ret[x, y] = (1 - abs(x / f - c)) * (1 - abs(y / f - c))
        return ret
    w = bilinear_conv_filler(filter_shape)
    w = np.repeat(w, ch * ch).reshape((filter_shape, filter_shape, ch, ch))

    weight_var = tf.constant(w, tf.float32,
                             shape=(filter_shape, filter_shape, ch, ch),
                             name='bilinear_upsample_filter')
    x = tf.pad(x, [[0, 0], [0, 0], [shape - 1, shape - 1], [shape - 1, shape - 1]], mode='SYMMETRIC')
    out_shape = tf.shape(x) * tf.constant([1, 1, shape, shape], tf.int32)
    deconv = tf.nn.conv2d_transpose(x, weight_var, out_shape,
                                    [1, 1, shape, shape], 'SAME', 'NCHW')
    edge = shape * (shape - 1)
    deconv = deconv[:, :, edge:-edge, edge:-edge,]

    if inp_shape[2]:
        inp_shape[2] *= shape
    if inp_shape[3]:
        inp_shape[3] *= shape
    deconv.set_shape(inp_shape)
    return deconv
####
@layer_register(use_scope=None)
def BNReLUx(x, name=None):
    """
    A shorthand of BatchNormalization + ReLU.
    """
    x = BatchNorm(name + '/bn', x)
    x = tf.nn.relu(x, name=name)
    return x
####
@layer_register(use_scope=None)
def BNScaleReLU(x, name=None):
    """
    A shorthand of BatchNormalization + Scale + ReLU.
    From densenet paper
    """
    # assume NCHW
    ch = x.get_shape().as_list()[1]
    x = BatchNorm('bn', x)
    with tf.variable_scope('bn/scale'):
        # across feature map
        gamma = tf.get_variable('gamma', [ch], initializer=tf.ones_initializer(), trainable=True)
        gamma = tf.reshape(gamma, [1, ch, 1, 1])
        beta = tf.get_variable('beta', [ch], initializer=tf.zeros_initializer(), trainable=True)
        beta = tf.reshape(beta, [1, ch, 1, 1])
        x = gamma * x + beta
    x = tf.nn.relu(x)
    return x
####
@layer_register(use_scope=None)
def Maxoutx(x, num_unit):
    """
    Patch for tensorpack's maxout, assume NCHW instead
    NOTE: recheck so that dont use transpose to swap
    """
    x = tf.transpose(x, [0, 2, 3, 1])
    input_shape = x.get_shape().as_list()
    ndim = len(input_shape)
    assert ndim == 4 or ndim == 2
    ch = input_shape[-1]
    assert ch is not None and ch % num_unit == 0
    out_chs = int(ch / num_unit)
    if ndim == 4:
        x = tf.reshape(x, [-1, input_shape[1], input_shape[2], out_chs, num_unit])
    else:
        x = tf.reshape(x, [-1, out_chs, num_unit])
    x = tf.reduce_max(x, ndim, name='output')
    x = tf.transpose(x, [0, 3, 1, 2])
    return x
####
@layer_register(use_scope=None)
def BNMaxout(x, num_unit, name=None):
    """
    A shorthand of BatchNormalization + Maxout.
    Input must be NCHW
    """
    name = "" if name is None else name
    x = BatchNorm(name + '/bn', x)
    x = Maxoutx(x, num_unit)
    return x
####
def MaxPoolIndice(name, x, kernel_size, stride, padding='SAME'):
    """
    Max-pooling layer. Downsamples values within a kernel to the 
    maximum value within the corresponding kernel
    Args:
        x: Input to the max-pooling layer, NCHW only
        kernel_size: Size of kernel where max-pooling is applied
        stride: Determines the downsample factor
        name: Name scope for operation
        padding: Same or valid padding
        index: Boolean- whether to return pooling indicies
    Return:
        pool: Tensor with max-pooling
        argmax: Indicies of maximal values computed in each kernel (use with segnet)
    """
    with tf.variable_scope(name):
        strides = [1,stride,stride,1]
        ksize = [1, kernel_size, kernel_size, 1]
        x = tf.transpose(x, [0, 2, 3, 1]) # NCHW -> NHWC
        x, argmax = tf.nn.max_pool_with_argmax(x, ksize, strides, padding=padding)
        x = tf.transpose(x, [0, 3, 1, 2]) # NHWC -> NCHW
    return x, argmax
####
def UnPoolIndice(name, x, ind, ksize=[1, 2, 2, 1]):
    """
    Unpooling layer after max_pool_with_argmax.
    Args:
        x: tensor to be unpooled
        ind: argmax indices
        ksize:ksize is the same as for the pool
    Return:
        unpool: unpooling tensor
    """
    with tf.variable_scope(name):
        shape_list = x.get_shape().as_list()
        x = tf.transpose(x, [0, 2, 3, 1]) # NCHW -> NHWC
        input_shape = tf.cast(tf.shape(x), tf.int64)
        output_shape = [input_shape[0], input_shape[1] * ksize[1], input_shape[2] * ksize[2], input_shape[3]]
        output_shape = tf.stack(output_shape)

        flat_input_size = tf.reduce_prod(input_shape)
        flat_output_shape = [output_shape[0], output_shape[1] * output_shape[2] * output_shape[3]]

        x_ = tf.reshape(x, [-1]) # simply flatten
        batch_range = tf.reshape(tf.range(output_shape[0], dtype=ind.dtype), shape=[-1, 1, 1, 1])
        b = tf.ones_like(ind) * batch_range
        b = tf.expand_dims(tf.reshape(b, [-1]), axis=-1)
        ind_ = tf.expand_dims(tf.reshape(ind, [-1]), axis=-1)
        ind_ = tf.concat([b, ind_], 1)

        ret = tf.scatter_nd(ind_, x_, shape=flat_output_shape)
        ret = tf.reshape(ret, shape=output_shape)
        ret = tf.transpose(ret, [0, 3, 1, 2]) # NHWC -> NCHW
        ####
        output_shape = shape_list
        output_shape[2] *= 2
        output_shape[3] *= 2
        ret.set_shape(output_shape)
    return ret
