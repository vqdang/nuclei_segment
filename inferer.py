import math
import cv2
import argparse
import numpy as np
import os
import sys

import tensorflow as tf

from tensorpack import *
from tensorpack.tfutils.symbolic_functions import *
from tensorpack.tfutils.summary import *

import glob
import re # regex
from scipy import io as sio
import matplotlib.pyplot as plt
from matplotlib import cm
from collections import deque

import json
import operator
from scipy.ndimage.interpolation import rotate

from config import *
from datagen.utils import *

####
def get_best_chkpts(path, metric_name, comparator='>'):
    """
    Args:
        path: chkpts directory, must contain "stats.json" file
    """
    stat_file = path + '/stats.json'
    ops = {
            '>': operator.gt,
            '<': operator.lt,
          }

    op_func = ops[comparator]
    with open(stat_file) as f:
        info = json.load(f)
    
    if comparator == '>':
        best_value  = -1000
    else:
        best_value  = 1.0e6

    best_chkpts = 0
    for epoch_stat in info:
        epoch_value = epoch_stat[metric_name]
        if op_func(epoch_value, best_value):
            best_value  = epoch_value
            best_chkpts = epoch_stat['global_step']
    best_chkpts = "%smodel-%d.index" % (path, best_chkpts)
    return best_chkpts
####
def gen_pred_map(x, win_size, msk_size, step_size, predictor, batch_size=16):
    """
    @param:
        x       : input image to be segmented. It will be split into patches
                  to run the prediction upon before being assembled back
        win_size: size of each input patch
        msk_size: size of output prediction applied on patch of win_size
        stp_size: step size to move, should be <= msk_size
    """    
    assert msk_size >= step_size, "Step size must be <= size of prediction output!!"    

    def get_last_steps(length, msk_size, step_size):
        nr_step = math.ceil((length - msk_size) / step_size)
        last_step = (nr_step + 1) * step_size
        return int(last_step), int(nr_step + 1)
    
    im_h = x.shape[0] 
    im_w = x.shape[1]

    last_h, nr_step_h = get_last_steps(im_h, msk_size[0], step_size[0])
    last_w, nr_step_w = get_last_steps(im_w, msk_size[1], step_size[1])

    diff_h = win_size[0] - step_size[0]
    padt = diff_h // 2
    padb = last_h + win_size[0] - im_h

    diff_w = win_size[1] - step_size[1]
    padl = diff_w // 2
    padr = last_w + win_size[1] - im_w

    x = np.lib.pad(x, ((padt, padb), (padl, padr), (0, 0)), 'reflect')

    ####
    sub_patches = []
    # generating subpatches from orginal
    for row in range(0, last_h, step_size[0]):
        for col in range (0, last_w, step_size[1]):
            win = x[row:row+win_size[0], 
                    col:col+win_size[1]]
            sub_patches.append(win)

    # if can fit normally generate batch
    pred_map = deque()
    while len(sub_patches) > batch_size:
        mini_batch  = sub_patches[:batch_size]
        sub_patches = sub_patches[batch_size:]
        mini_output = predictor(mini_batch)[0]
        mini_output = np.split(mini_output, batch_size, axis=0)
        pred_map.extend(mini_output)
    if len(sub_patches) != 0:
        mini_output = predictor(sub_patches)[0]
        mini_output = np.split(mini_output, len(sub_patches), axis=0)
        pred_map.extend(mini_output)

    #### Assemble back into full image
    y = np.zeros([last_h + msk_size[0],
                  last_w + msk_size[1]],
                  dtype=np.float32)

    for row in range(0, last_h, step_size[0]):
        for col in range (0, last_w, step_size[1]):
            win = pred_map.popleft()
            win = np.squeeze(win)
            y[row:row+msk_size[0], 
              col:col+msk_size[1]] = win
    y = y[:im_h,:im_w] # just crop back to original size

    return y
####
def run(desc, gen_tta=False):
    if desc.inf_manual_chkpts:
        model_path = desc.inf_model_path
    else:
        model_path = get_best_chkpts(desc.save_dir, 
                                     desc.inf_eval_metric,
                                     desc.inf_comparator)

    pred_config = PredictConfig(
        model=Model(desc),
        session_init=get_model_loader(model_path),
        input_names=['image'],
        output_names=desc.pred_tensors)
    predictor = OfflinePredictor(pred_config)

    imgs_dir = desc.inf_imgs_dir
    imgs_ext = desc.inf_imgs_ext
    file_list = glob.glob(imgs_dir + '/*%s' % imgs_ext)
    file_list.sort() # ensure same order [1]

    save_dir = desc.inf_output_dir
    norm_root_dir = desc.inf_norm_dir
    norm_codes = desc.inf_norm_codes

    for norm_target in norm_codes:
        norm_dir = '%s/%s/' % (norm_root_dir, norm_target)
        norm_save_dir = '%s/%s/' % (save_dir, norm_target)
        rm_n_mkdir(norm_save_dir)
        for filename in file_list: # png for base
            filename = os.path.basename(filename)
            basename = filename.split('.')[0]
            print(basename, norm_target)

            ##
            img = cv2.imread(norm_dir + filename)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

            imgs_list = [img]
            if gen_tta:
                imgs_list.append(np.flipud(img)) 
                imgs_list.append(np.fliplr(img)) 
                imgs_list.append(rotate(img,  90)) 
                imgs_list.append(rotate(img, 180))
                imgs_list.append(rotate(img, 270))
                
            ##
            pred_list = []
            for img in imgs_list:
                predmap = gen_pred_map(img, 
                                        desc.valid_base_shape, 
                                        desc.valid_mask_shape,
                                        desc.valid_mask_shape,
                                        predictor)
                pred_list.append(predmap)

            # turn back to original coord
            if gen_tta:
                pred_list[1] = np.flipud(pred_list[1]) 
                pred_list[2] = np.fliplr(pred_list[2]) 
                pred_list[3] = rotate(pred_list[3], - 90) 
                pred_list[4] = rotate(pred_list[4], -180)
                pred_list[5] = rotate(pred_list[5], -270)
            sio.savemat('%s/%s.mat' % (norm_save_dir, basename), {'result':pred_list})
####
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', help='comma separated list of GPU(s) to use.')
    parser.add_argument('--tta', action='store_true', help='make a set of prediction for tta')
    args = parser.parse_args()
        
    if args.gpu:
        os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    n_gpus = len(args.gpu.split(','))

    desc = Config()
    run(desc, args.tta)
    