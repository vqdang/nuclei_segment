
import os
import glob
import cv2
import shutil
import math
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

import staintools
from datagen.utils import rm_n_mkdir

###

# norm_brightness = False # simon didnt use
# imgs_dir = '../../data/NUC_Kumar/test-set/imgs_orig/' 
# save_dir = '../../data/NUC_Kumar/test-set/imgs_norm/' 
# targets_dir = '../../data/NUC_Kumar/train-set/imgs_orig/'

# file_list = glob.glob(imgs_dir + '*.tif')
# file_list.sort() # ensure same order [1]

# if norm_brightness:
#     standardizer = staintools.BrightnessStandardizer()
# stain_normalizer = staintools.StainNormalizer(method='vahadane')

# stain_norm_target = \
#         {"TCGA-AR-A1AS-01Z-00-DX1":'A1AS',
#          "TCGA-21-5784-01Z-00-DX1":'5784',
#          "TCGA-DK-A2I6-01A-01-TS1":'A2I6',
#          "TCGA-A7-A13E-01Z-00-DX1":'A13E',}

# for target_name, target_code in stain_norm_target.items():
#     target_img = cv2.imread('%s/%s.tif' % (targets_dir, target_name))
#     target_img = cv2.cvtColor(target_img, cv2.COLOR_BGR2RGB)
#     if norm_brightness:
#         target_img = standardizer.transform(target_img)
#     stain_normalizer.fit(target_img)
    
#     norm_dir = "%s/%s/" % (save_dir, target_code)
#     rm_n_mkdir(norm_dir)
    
#     for img_path in file_list:
#         filename = os.path.basename(img_path)
#         basename = filename.split('.')[0]
#         print(basename)
        
#         img = cv2.imread(img_path)
#         img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
#         if norm_brightness:
#             img = standardizer.transform(img)
#         img = stain_normalizer.transform(img)

#         img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
#         cv2.imwrite("%s/%s.tif" % (norm_dir, basename), img)

###
# i = tf.ones([2, 3, 256, 256])
# shape = tf.shape(i)
# batch = shape[0]
# x_range = tf.range(1, shape[2]+1)
# y_range = tf.range(1, shape[3]+1)
# x, y = tf.meshgrid(x_range, y_range)

# x = tf.expand_dims(x, axis=0) # depth
# x = tf.expand_dims(x, axis=0) # batch
# x = tf.tile(x, [batch, 1, 1, 1])

# y = tf.expand_dims(y, axis=0) # depth
# y = tf.expand_dims(y, axis=0) # batch
# y = tf.tile(y, [batch, 1, 1, 1])

# x = tf.cast(x, tf.float32)
# y = tf.cast(y, tf.float32)
# # shift xy to center
# pi = tf.constant(math.pi)
# nx =  x - tf.cast(shape[2], tf.float32) / 2 + 0.5
# ny = -y + tf.cast(shape[3], tf.float32) / 2 + 0.5
# r = tf.sqrt(nx * nx + ny * ny)
# a = tf.atan(tf.abs(ny / nx))
# a = tf.where(tf.logical_and(nx >  0, ny >  0),        a, a) # q1
# a = tf.where(tf.logical_and(nx <= 0, ny >  0), pi   - a, a) # q2
# a = tf.where(tf.logical_and(nx <= 0, ny <= 0), pi   + a, a) # q3
# a = tf.where(tf.logical_and(nx >  0, ny <  0), 2*pi - a, a) # q4
# a = tf.where(tf.is_nan(a), tf.zeros_like(a), a)

# x = tf.transpose(x, [0, 2, 3, 1])
# y = tf.transpose(y, [0, 2, 3, 1])
# r = tf.transpose(r, [0, 2, 3, 1])
# a = tf.transpose(a*360/2/pi, [0, 2, 3, 1])
# nx = tf.transpose(nx, [0, 2, 3, 1])
# ny = tf.transpose(ny, [0, 2, 3, 1])

# with tf.Session() as sess:
#     x, y, nx, ny, r, a = sess.run([x, y, nx, ny, r, a])

# print(y.shape)
# print(x[0,...,0])
# print(y[0,...,0])
# print(nx[0,...,0])
# print(ny[0,...,0])
# print(r[0,...,0])
# print(a[0,...,0])

# np.set_printoptions(precision=2)

# pi = math.pi

# x = np.ones([256, 256])
# y = np.ones([256, 256])

# for i in range(x.shape[0]):
#     for j in range(x.shape[1]):
#         x[i,j] = j+1

# for i in range(x.shape[0]):
#     for j in range(x.shape[1]):
#         y[i,j] = i+1

# x =  x - (np.amax(x) / 2.0 + 0.5)
# y = -y + (np.amax(y) / 2.0 + 0.5)
# rho = np.sqrt(x**2 + y**2)
# phi = np.arctan2(y, x)
# phi = phi * 360 / 2 / pi + 180.0
# phi = np.flipud(phi)
# phi = np.fliplr(phi)
# print(x)
# print(y)
# print(rho)
# print(phi)

# plt.subplot(1,2,1)
# plt.imshow(phi)
# plt.subplot(1,2,2)
# plt.imshow(a[0,...,0])
# plt.show()