
import glob
from config import *
import dataloader.dataset as loader

###########################################
def get_files(data_dir, data_ext):
    if not isinstance(data_dir, (list,)):
        data_files = glob.glob(data_dir + '/*' + data_ext)
    else:
        data_files = []
        for sub_dir in data_dir:
            files = glob.glob(sub_dir + '/*'+ data_ext)
            data_files.extend(files)
    return data_files
####
def calculate_mean_variance_image(list_images):
    """
    ported from simon
    """
    image = list_images[0]
    if np.random.randint(2, size=1) == 1:
        image = np.flipud(image)
    if np.random.randint(2, size=1) == 1:
        image = np.fliplr(image)
    image = np.float32(image)

    mean_image = image
    variance_image = np.zeros(shape = image.shape, dtype = np.float32)

    for t, image in enumerate(list_images[1:]):

        if np.random.randint(2, size=1) == 1:
            image = np.flipud(image)
        if np.random.randint(2, size=1) == 1:
            image = np.fliplr(image)
        image = np.float32(image)

        mean_image = (np.float32(t + 1)*mean_image + image)/np.float32(t+2)

        variance_image = np.float32(t+1)/np.float32(t+2)*variance_image \
                            + np.float32(1)/np.float32(t+1)*((image - mean_image)**2)

    return mean_image, variance_image
###
def train(args):

    if not args.view:
        os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
        nr_gpus = len(args.gpu.split(','))

    ####
    random.seed(5) 
    desc = Config()

    ####
    train_files = get_files(desc.train_dir, desc.data_ext)
    train_dataset = loader.DatasetSerial(train_files)
    train_datagen = loader.train_generator(train_dataset,
                                        desc.train_base_shape,
                                        desc.train_mask_shape, 
                            batch_size = desc.train_batch_size)

    ## Debugging purpose
    if args.view:
        loader.view_data_serial(train_datagen)
        return

    ## FIXME
    # if precomp_norm:
    #     mu_img, va_img = calculate_mean_variance_image(patch_list)
    #     sio.savemat("%s/%s_train_stats.mat" % (out_root_path, extract_type) ,
    #                     {'mean_image':mu_img, 'variance_image':va_img})        

    ###### must be called before ModelSaver
    logger.set_logger_dir(desc.save_dir)

    lr_sched = []
    cur_lr = desc.init_lr
    for i in range(1, desc.nr_epochs):
        if i % desc.lr_steps != 0:
            continue 
        new_lr = cur_lr / desc.lr_factor
        lr_sched.append((i, new_lr))
        cur_lr = new_lr

    callbacks=[
            ModelSaver(max_to_keep=200),
            ScheduledHyperParamSetter('learning_rate', lr_sched),                                                      
            MergeAllSummaries()
            ]

    ######
    if desc.do_valid:
        #####
        valid_files = get_files(desc.valid_dir, desc.data_ext)
        valid_dataset = loader.DatasetSerial(valid_files)
        valid_datagen = loader.valid_generator(valid_dataset,
                                            desc.valid_base_shape,
                                            desc.valid_mask_shape, 
                               batch_size = desc.valid_batch_size)

        # multi-GPU inference (with mandatory queue prefetch)
        infs = [StatCollector(desc)]
        callbacks.append(DataParallelInferenceRunner(
                                valid_datagen, infs, list(range(nr_gpus))))

    ######
    steps_per_epoch = train_datagen.size() // nr_gpus

    config = TrainConfig(
                model           = Model(desc, steps_per_epoch),
                callbacks       = callbacks      ,
                dataflow        = train_datagen  ,
                steps_per_epoch = steps_per_epoch,
                max_epoch       = desc.nr_epochs ,
            )

    if desc.pretrained_path is not None:
        if desc.load_mode == 'new':
            config.session_init = get_model_loader(desc.pretrained_path)
        else:
            config.session_init = SaverRestore(desc.pretrained_path, ignore=['learning_rate'])

    launch_train_with_config(config, SyncMultiGPUTrainerParameterServer(nr_gpus))

####

###########################################################################

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', help='comma separated list of GPU(s) to use.')
    parser.add_argument('--load', help='load model')
    parser.add_argument('--view', help='view dataset', action='store_true')
    args = parser.parse_args()

    train(args)

