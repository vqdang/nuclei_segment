import numpy as np 
import matplotlib.pyplot as plt
import cv2
import glob
import random
import pandas as pd
import os 
import matplotlib.cm as cm
from scipy.ndimage.filters import gaussian_filter
from scipy.ndimage.interpolation import map_coordinates, affine_transform
from scipy import ndimage
import rotate
import imutils
import math

'''
Author: Simon Graham
Tissue Image Analytics Lab
Department of Computer Science, 
University of Warwick, UK.
'''

def add_gaussian_noise(image_in, noise_sigma):
    temp_image = np.float64(np.copy(image_in))

    h = temp_image.shape[0]
    w = temp_image.shape[1]
    noise = np.random.randn(h, w) * noise_sigma

    noisy_image = np.zeros(temp_image.shape, np.float64)
    if len(temp_image.shape) == 2:
        noisy_image = temp_image + noise
    else:
        noisy_image[:,:,0] = temp_image[:,:,0] + noise
        noisy_image[:,:,1] = temp_image[:,:,1] + noise
        noisy_image[:,:,2] = temp_image[:,:,2] + noise

    """
    print('min,max = ', np.min(noisy_image), np.max(noisy_image))
    print('type = ', type(noisy_image[0][0][0]))
    """

    return noisy_image

def elastic_transform(image, alpha, sigma, random_state=None):
    """Elastic deformation of images as described in [Simard2003]_.
    .. [Simard2003] Simard, Steinkraus and Platt, "Best Practices for
       Convolutional Neural Networks applied to Visual Document Analysis", in
       Proc. of the International Conference on Document Analysis and
       Recognition, 2003.
    """
    if random_state is None:
        random_state = np.random.RandomState(None)

    shape = image.shape
    dx = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant", cval=0) * alpha
    dy = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant", cval=0) * alpha
    dz = np.zeros_like(dx)

    x, y, z = np.meshgrid(np.arange(shape[0]), np.arange(shape[1]), np.arange(shape[2]))
    indices = np.reshape(y+dy, (-1, 1)), np.reshape(x+dx, (-1, 1)), np.reshape(z, (-1, 1))

    distored_image = map_coordinates(image, indices, order=1, mode='reflect')
    return distored_image.reshape(image.shape)

######################################################################################
train_root = '/home/simon/Desktop/nuclei_data/kumar/Kumar_NS/Training/Images/'
train_image_out = '/home/simon/Desktop/nuclei_data/kumar/Kumar_NS/Training/augmentation/Images1/'
train_gt_out = '/home/simon/Desktop/nuclei_data/kumar/Kumar_NS/Training/augmentation/Labels1/'
train_c_out = '/home/simon/Desktop/nuclei_data/kumar/Kumar_NS/Training/augmentation/Contours1/'

train_images_list = glob.glob(train_root + 'TC*')

num_train = len(train_images_list)

for i in range(num_train):
	image_file = train_images_list[i]
	basename = os.path.basename(image_file)
	basename = basename.split('.')[0]
	im = cv2.imread(image_file)
	image_height = im.shape[0]
	image_width = im.shape[1]
	
	square_idx = np.argmin([im.shape[0], im.shape[1]])
	square_max = max(im.shape[0], im.shape[1])
	zeros = np.zeros([square_max,square_max,3])
	zeros[:im.shape[0], :im.shape[1],:] = im
	zeros = zeros.astype('uint8')

	im_blur = cv2.GaussianBlur(im,(7,7),0)
	im_blur2 = cv2.medianBlur(im, 5, 0)
	im_noise1 = add_gaussian_noise(im, 6)
	im_noise2 = add_gaussian_noise(im, 10)


	im_scale1 = cv2.resize(im,None,fx=1.8, fy=1.8, interpolation = cv2.INTER_CUBIC)
	im_scale2 = cv2.resize(im,None,fx=0.6, fy=0.6, interpolation = cv2.INTER_AREA)
	im_scale3 = cv2.resize(im,None,fx=1.4, fy=1.4, interpolation = cv2.INTER_CUBIC)
	im_scale4 = cv2.resize(im,None,fx=0.8, fy=0.8, interpolation = cv2.INTER_AREA)
	elastic_c2 = elastic_transform(zeros, 900,10)
	elastic_c2 = elastic_c2[:im.shape[0], :im.shape[1],:]
	im_elastic1_2 = elastic_c2[10:elastic_c2.shape[0]-10,10:elastic_c2.shape[1]-10,:]
	elastic_c3 = elastic_transform(zeros, 900,9)
	elastic_c3 = elastic_c3[:im.shape[0], :im.shape[1],:]
	im_elastic1_3 = elastic_c3[10:elastic_c3.shape[0]-10,10:elastic_c3.shape[1]-10,:]
	elastic_c4 = elastic_transform(zeros, 900,8)
	elastic_c4 = elastic_c4[:im.shape[0], :im.shape[1],:]
	im_elastic1_4 = elastic_c4[10:elastic_c4.shape[0]-10,10:elastic_c4.shape[1]-10,:]

	ground_truth = cv2.imread('/home/simon/Desktop/nuclei_data/kumar/Kumar_NS/Training/Labels/' + basename + '.png',0)
	ret,ground_truth = cv2.threshold(ground_truth,0,1,cv2.THRESH_BINARY)
	gt_scale1 = cv2.resize(ground_truth,None,fx=1.8, fy=1.8, interpolation = cv2.INTER_CUBIC)
	ret_,gt_scale1 = cv2.threshold(gt_scale1,0,1,cv2.THRESH_BINARY)
	gt_scale2 = cv2.resize(ground_truth,None,fx=0.6, fy=0.6, interpolation = cv2.INTER_AREA)
	ret__,gt_scale2 = cv2.threshold(gt_scale2,0,1,cv2.THRESH_BINARY)
	gt_scale3 = cv2.resize(ground_truth,None,fx=1.4, fy=1.4, interpolation = cv2.INTER_CUBIC)
	ret___,gt_scale3 = cv2.threshold(gt_scale3,0,1,cv2.THRESH_BINARY)
	gt_scale4 = cv2.resize(ground_truth,None,fx=0.8, fy=0.8, interpolation = cv2.INTER_AREA)
	ret____,gt_scale4 = cv2.threshold(gt_scale4,0,1,cv2.THRESH_BINARY)
	zeros2 = np.zeros([square_max,square_max])
	zeros2[:im.shape[0], :im.shape[1]] = ground_truth
	ground_truth2_1 = zeros2
	ground_truth2_1 = np.expand_dims(ground_truth2_1,axis=2)
	gt_elastic1_2 = elastic_transform(ground_truth2_1, 900, 10)
	gt_elastic1_2 = np.squeeze(gt_elastic1_2)
	gt_elastic1_2 = gt_elastic1_2[:im.shape[0], :im.shape[1]]
	gt_elastic1_2 = gt_elastic1_2[10:gt_elastic1_2.shape[0]-10,10:gt_elastic1_2.shape[1]-10]
	_1, gt_elastic1_2 = cv2.threshold(gt_elastic1_2, 0.5, 1, cv2.THRESH_BINARY)
	gt_elastic1_3 = elastic_transform(ground_truth2_1, 900, 9)
	gt_elastic1_3 = np.squeeze(gt_elastic1_3)
	gt_elastic1_3 = gt_elastic1_3[:im.shape[0], :im.shape[1]]
	gt_elastic1_3 = gt_elastic1_3[10:gt_elastic1_3.shape[0]-10,10:gt_elastic1_3.shape[1]-10]
	_2, gt_elastic1_3 = cv2.threshold(gt_elastic1_3, 0.5, 1, cv2.THRESH_BINARY)
	gt_elastic1_4 = elastic_transform(ground_truth2_1, 900, 8)
	gt_elastic1_4 = np.squeeze(gt_elastic1_4)
	gt_elastic1_4 = gt_elastic1_4[:im.shape[0], :im.shape[1]]
	gt_elastic1_4 = gt_elastic1_4[10:gt_elastic1_4.shape[0]-10,10:gt_elastic1_4.shape[1]-10]
	_3, gt_elastic1_4 = cv2.threshold(gt_elastic1_4, 0.5, 1, cv2.THRESH_BINARY)

	contours = cv2.imread('/home/simon/Desktop/nuclei_data/kumar/Kumar_NS/Training/Contours/' + basename + '.png',0)
	ret,contours = cv2.threshold(contours,0,1,cv2.THRESH_BINARY)
	contours_scale1 = cv2.resize(contours,None,fx=1.8, fy=1.8, interpolation = cv2.INTER_CUBIC)
	retc_,contours_scale1 = cv2.threshold(contours_scale1,0,1,cv2.THRESH_BINARY)
	contours_scale2 = cv2.resize(contours,None,fx=0.6, fy=0.6, interpolation = cv2.INTER_AREA)
	retc__,contours_scale2 = cv2.threshold(contours_scale2,0,1,cv2.THRESH_BINARY)
	contours_scale3 = cv2.resize(contours,None,fx=1.4, fy=1.4, interpolation = cv2.INTER_CUBIC)
	retc___,contours_scale3 = cv2.threshold(contours_scale3,0,1,cv2.THRESH_BINARY)
	contours_scale4 = cv2.resize(contours,None,fx=0.8, fy=0.8, interpolation = cv2.INTER_AREA)
	retc____,contours_scale4 = cv2.threshold(contours_scale4,0,1,cv2.THRESH_BINARY)
	zeros2 = np.zeros([square_max,square_max])
	zeros2[:im.shape[0], :im.shape[1]] = contours
	contours2_1 = zeros2
	contours2_1 = np.expand_dims(contours2_1,axis=2)
	contours1_2 = elastic_transform(contours2_1, 900, 10)
	contours1_2 = np.squeeze(contours1_2)
	contours1_2 = contours1_2[:im.shape[0], :im.shape[1]]
	contours1_2 = contours1_2[10:contours1_2.shape[0]-10,10:contours1_2.shape[1]-10]
	_1, contours1_2 = cv2.threshold(contours1_2, 0.5, 1, cv2.THRESH_BINARY)
	contours1_3 = elastic_transform(contours2_1, 900, 9)
	contours1_3 = np.squeeze(contours1_3)
	contours1_3 = contours1_3[:im.shape[0], :im.shape[1]]
	contours1_3 = contours1_3[10:contours1_3.shape[0]-10,10:contours1_3.shape[1]-10]
	_2, contours1_3 = cv2.threshold(contours1_3, 0.5, 1, cv2.THRESH_BINARY)
	contours1_4 = elastic_transform(contours2_1, 900, 8)
	contours1_4 = np.squeeze(contours1_4)
	contours1_4 = contours1_4[:im.shape[0], :im.shape[1]]
	contours1_4 = contours1_4[10:contours1_4.shape[0]-10,10:contours1_4.shape[1]-10]
	_3, contours1_4 = cv2.threshold(contours1_4, 0.5, 1, cv2.THRESH_BINARY)

	rand_rot1 =np.random.randint(10,350)
	rand_rot2 =np.random.randint(10,350)
	rand_rot3 =np.random.randint(10,350)
	rand_rot4 =np.random.randint(10,350)
	rand_rot5 =np.random.randint(10,350)


	image_rotated1 = rotate.rotate_image(im, rand_rot1)
	image_rotated_cropped1 = rotate.crop_around_center(image_rotated1,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot1)))
	label_rotated1 = rotate.rotate_image(ground_truth,rand_rot1)
	label_rotated_cropped1 = rotate.crop_around_center(label_rotated1,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot1)))
	contours_rotated1 = rotate.rotate_image(contours,rand_rot1)
	contours_rotated_cropped1 = rotate.crop_around_center(contours_rotated1,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot1)))

	image_rotated2 = rotate.rotate_image(im,rand_rot2)
	image_rotated_cropped2 = rotate.crop_around_center(image_rotated2,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot2)))
	label_rotated2 = rotate.rotate_image(ground_truth,rand_rot2)
	label_rotated_cropped2 = rotate.crop_around_center(label_rotated2,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot2)))
	contours_rotated2 = rotate.rotate_image(contours,rand_rot2)
	contours_rotated_cropped2 = rotate.crop_around_center(contours_rotated2,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot2)))
	
	image_rotated3 = rotate.rotate_image(im,rand_rot3)
	image_rotated_cropped3 = rotate.crop_around_center(image_rotated3,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot3)))
	label_rotated3 = rotate.rotate_image(ground_truth,rand_rot3)
	label_rotated_cropped3 = rotate.crop_around_center(label_rotated3,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot3)))
	contours_rotated3 = rotate.rotate_image(contours,rand_rot3)
	contours_rotated_cropped3 = rotate.crop_around_center(contours_rotated3,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot3)))
	
	image_rotated4 = rotate.rotate_image(im,rand_rot4)
	image_rotated_cropped4 = rotate.crop_around_center(image_rotated4,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot4)))
	label_rotated4 = rotate.rotate_image(ground_truth,rand_rot4)
	label_rotated_cropped4 = rotate.crop_around_center(label_rotated4,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot4)))
	contours_rotated4 = rotate.rotate_image(contours,rand_rot4)
	contours_rotated_cropped4 = rotate.crop_around_center(contours_rotated4,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot4)))

	image_rotated5 = rotate.rotate_image(im,rand_rot5)
	image_rotated_cropped5 = rotate.crop_around_center(image_rotated5,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot5)))
	label_rotated5 = rotate.rotate_image(ground_truth,rand_rot5)
	label_rotated_cropped5 = rotate.crop_around_center(label_rotated5,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot5)))
	contours_rotated5 = rotate.rotate_image(contours,rand_rot5)
	contours_rotated_cropped5 = rotate.crop_around_center(contours_rotated5,*rotate.largest_rotated_rect(image_width,image_height,math.radians(rand_rot5)))

	cv2.imwrite(train_image_out + basename + '.png', im)
	cv2.imwrite(train_gt_out + basename + '.png', ground_truth)
	cv2.imwrite(train_c_out + basename + '.png', contours)
	cv2.imwrite(train_image_out + basename + '_scale1.png', im_scale1)
	cv2.imwrite(train_gt_out + basename + '_scale1.png', gt_scale1)
	cv2.imwrite(train_c_out + basename + '_scale1.png', contours_scale1)
	cv2.imwrite(train_image_out + basename + '_scale2.png', im_scale2)
	cv2.imwrite(train_gt_out + basename + '_scale2.png', gt_scale2)
	cv2.imwrite(train_c_out + basename + '_scale2.png', contours_scale2)
	cv2.imwrite(train_image_out + basename + '_scale3.png', im_scale3)
	cv2.imwrite(train_gt_out + basename + '_scale3.png', gt_scale3)
	cv2.imwrite(train_c_out + basename + '_scale3.png', contours_scale3)
	cv2.imwrite(train_image_out + basename + '_scale4.png', im_scale4)
	cv2.imwrite(train_gt_out + basename + '_scale4.png', gt_scale4)
	cv2.imwrite(train_c_out + basename + '_scale4.png', contours_scale4)
	cv2.imwrite(train_image_out + basename + '_blur.png',im_blur)
	cv2.imwrite(train_gt_out + basename + '_blur.png',ground_truth)
	cv2.imwrite(train_c_out + basename + '_blur.png', contours)
	cv2.imwrite(train_image_out + basename + '_blur2.png',im_blur2)
	cv2.imwrite(train_gt_out + basename + '_blur2.png',ground_truth)
	cv2.imwrite(train_c_out + basename + '_blur2.png', contours)
	cv2.imwrite(train_image_out + basename + '_noise1.png',im_noise1)
	cv2.imwrite(train_gt_out + basename + '_noise1.png',ground_truth)
	cv2.imwrite(train_c_out + basename + '_noise1.png', contours)
	cv2.imwrite(train_image_out + basename + '_noise2.png',im_noise2)
	cv2.imwrite(train_gt_out + basename + '_noise2.png',ground_truth)
	cv2.imwrite(train_c_out + basename + '_noise2.png', contours)
	cv2.imwrite(train_image_out + basename + 'im_rot1.png',image_rotated_cropped1)
	cv2.imwrite(train_gt_out + basename + 'im_rot1.png', label_rotated_cropped1)
	cv2.imwrite(train_c_out + basename + 'im_rot1.png', contours_rotated_cropped1)
	cv2.imwrite(train_image_out + basename + 'im_rot2.png', image_rotated_cropped2)
	cv2.imwrite(train_gt_out + basename + 'im_rot2.png', label_rotated_cropped2)
	cv2.imwrite(train_c_out + basename + 'im_rot2.png', contours_rotated_cropped2)
	cv2.imwrite(train_image_out + basename + 'im_rot3.png', image_rotated_cropped3)
	cv2.imwrite(train_gt_out + basename + 'im_rot3.png', label_rotated_cropped3)
	cv2.imwrite(train_c_out + basename + 'im_rot3.png', contours_rotated_cropped3)
	cv2.imwrite(train_image_out + basename + 'im_rot4.png', image_rotated_cropped4)
	cv2.imwrite(train_gt_out + basename + 'im_rot4.png', label_rotated_cropped4)
	cv2.imwrite(train_c_out + basename + 'im_rot4.png', contours_rotated_cropped4)
	cv2.imwrite(train_image_out + basename + 'im_rot5.png', image_rotated_cropped5)
	cv2.imwrite(train_gt_out + basename + 'im_rot5.png', label_rotated_cropped5)
	cv2.imwrite(train_c_out + basename + 'im_rot5.png', contours_rotated_cropped5)
	cv2.imwrite(train_image_out + basename + 'im_elastic1.png', im_elastic1_2)
	cv2.imwrite(train_gt_out + basename + 'im_elastic1.png', gt_elastic1_2)
	cv2.imwrite(train_c_out + basename + 'im_elastic1.png', contours1_2)
	cv2.imwrite(train_image_out + basename + 'im_elastic2.png', im_elastic1_3)
	cv2.imwrite(train_gt_out + basename + 'im_elastic2.png', gt_elastic1_3)
	cv2.imwrite(train_c_out + basename + 'im_elastic2.png', contours1_3)
	cv2.imwrite(train_image_out + basename + 'im_elastic3.png', im_elastic1_4)
	cv2.imwrite(train_gt_out + basename + 'im_elastic3.png', gt_elastic1_4)
	cv2.imwrite(train_c_out + basename + 'im_elastic3.png', contours1_4)