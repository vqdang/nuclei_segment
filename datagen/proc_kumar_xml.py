
##
## For processing .xml annotation file in Weebly dataset
##

import colorsys
import glob
import math
import os
import random
import re  # regex
import shutil
import xml.etree.ElementTree as ET

import cv2
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from scipy import misc, ndimage
from scipy import io as sio
from scipy.ndimage.measurements import label as blob_labelling
from scipy.ndimage.morphology import binary_dilation, distance_transform_edt

####
def random_colors(N, bright=True):
    """
    Generate random colors.
    To get visually distinct colors, generate them in HSV space then
    convert to RGB.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    random.shuffle(colors)
    return colors
####
def visualize_instances(insts_list, canvas=None, color=None):

    canvas = np.full(mask.shape + (3,), 200, dtype=np.uint8) \
                if canvas is None else np.copy(canvas)

    inst_colors = random_colors(len(insts_list))
    inst_colors = np.array(inst_colors) * 255

    for idx, inst_map in enumerate(insts_list):
        inst_color = color if color is not None else inst_colors[idx]
        contours = cv2.findContours(inst_map.copy(), 
                                cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        cv2.drawContours(canvas, contours[1], -1, inst_color, 2)
    return canvas
####
def gen_figure(imgs_list, titles, fig_inch, show=False, colormap=cm.jet):

    num_img = len(imgs_list)
    nrows = math.ceil(math.sqrt(num_img))
    ncols = math.ceil(num_img / nrows)

    # generate figure
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, 
                             sharex='all', sharey='all')
    axes = [axes] if nrows == 1 else axes

    # not very elegant
    idx = 0
    for ax in axes:
        for cell in ax:
            cell.set_title(titles[idx])
            cell.imshow(imgs_list[idx], cmap=colormap)
            cell.tick_params(axis='both', 
                            which='both', 
                            bottom='off', 
                            top='off', 
                            labelbottom='off', 
                            right='off', 
                            left='off', 
                            labelleft='off')
            idx += 1
            if idx == len(titles):
                break
        if idx == len(titles):
            break
 
    fig.tight_layout()
    return fig
####
def get_centroid(vertices):
    length = vertices.shape[0]
    sum_x = np.sum(vertices[:, 0])
    sum_y = np.sum(vertices[:, 1])
    cen_x = int(sum_x / length + 0.5)
    cen_y = int(sum_y / length + 0.5)
    return (cen_x, cen_y)
####

imgs_dir = "/home/vqdang/work/data/NUC_Kumar/imgs/"
anns_dir = "/home/vqdang/work/data/NUC_Kumar/anns/" # .xml folders
proc_dir = "/home/vqdang/work/data/NUC_Kumar/msks_fixed/imgs_train/"

file_list = glob.glob(imgs_dir + '*')
file_list.sort() # ensure same order [1]

test_list = ["TCGA-AY-A8YK-01A-01-TS1",
             "TCGA-G2-A2EK-01A-02-TSB",
             "TCGA-NH-A8F7-01A-01-TS1",
             "TCGA-DK-A2I6-01A-01-TS1",
             "TCGA-KB-A93J-01A-01-TS1",
             "TCGA-RD-A8N9-01A-01-TS1",
             "TCGA-21-5786-01Z-00-DX1",
             "TCGA-B0-5698-01Z-00-DX1",
             "TCGA-E2-A1B5-01Z-00-DX1",
             "TCGA-49-4488-01Z-00-DX1",
             "TCGA-B0-5710-01Z-00-DX1",
             "TCGA-G9-6336-01Z-00-DX1",
             "TCGA-A7-A13F-01Z-00-DX1",
             "TCGA-CH-5767-01Z-00-DX1",]

if (os.path.isdir(proc_dir)):
    shutil.rmtree(proc_dir)
os.makedirs(proc_dir)

for filename in file_list: # png for base
    filename = os.path.basename(filename)
    basename = filename.split('.')[0]

    if basename in test_list:
        continue

    print(basename)
    img = cv2.imread(imgs_dir + filename)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    hw = img.shape[:2]

    xml = ET.parse(anns_dir + basename + '.xml')

    contour_dbg = np.zeros(hw, np.uint8)

    insts_list = []
    for idx, region_xml in enumerate(xml.findall('.//Region')):
        vertices = []
        for vertex_xml in region_xml.findall('.//Vertex'):
            attrib = vertex_xml.attrib
            vertices.append([float(attrib['X']), 
                                float(attrib['Y'])])
        vertices = np.array(vertices) + 0.5
        vertices = vertices.astype('int32')
        centroid = get_centroid(vertices)
        contour_blb = np.zeros(hw, np.uint8)
        # fill both the inner area and contour with idx+1 color
        cv2.drawContours(contour_blb, [vertices], 0, idx+1, -1)
        cv2.drawContours(contour_dbg, [vertices], 0, 255,  1) # for debug
        insts_list.append(contour_blb)

    insts_size_list = np.array(insts_list)
    insts_size_list = np.sum(insts_size_list, axis=(1 , 2))
    insts_size_list = list(insts_size_list)

    pair_insts_list = zip(insts_list, insts_size_list)
    # sort in z-axis basing on size, larger on top
    pair_insts_list = sorted(pair_insts_list, key=lambda x: x[1])
    insts_list, insts_size_list = zip(*pair_insts_list)

    ann = np.zeros(hw, np.int32)
    for idx, inst_map in enumerate(insts_list):
        ann[inst_map > 0] = idx + 1

    overlay_ann = visualize_instances(insts_list, img)

    np.save('%s/%s.npy' % (proc_dir, basename), ann)
    sio.savemat('%s/%s.mat' % (proc_dir, basename), 
                    {'result':ann, 'overlaid':overlay_ann})

    ##
    # if drawn blob misses boundary pixel, the value will be 255
    # else it will be idx + 1 +255 on the boundary
    #
    # overlay_ann = visualize_instances(insts_list, img)
    # imgs_list = [img, overlay_ann, ann, canvas+contour_dbg]
    # imgs_title = ['Input', 'Over-True', 'True', 'fixed']
    # comparison_figure = gen_figure(imgs_list, imgs_title, (8, 8), colormap=cm.jet)
    # plt.show()
