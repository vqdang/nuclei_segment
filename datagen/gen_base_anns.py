
import colorsys
import glob
import math
import os
import random
import re  # regex
import shutil

import cv2
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
from matplotlib import cm
from scipy import misc, ndimage
from scipy.ndimage import measurements
from scipy.ndimage.morphology import (binary_dilation, distance_transform_cdt,
                                      distance_transform_edt)
from skimage.morphology import skeletonize_3d

from utils import rm_n_mkdir


####
def gen_dst_map(ann, map_type='chessboard', skeleton_norm=True):  
    shape = ann.shape[:2] # HW
    nuc_list = list(np.unique(ann))
    nuc_list.remove(0) # 0 is background

    canvas = np.zeros(shape, dtype=np.uint8)
    distance_transform = distance_transform_cdt \
                        if map_type == 'chessboard' \
                        else distance_transform_edt

    for nuc_id in nuc_list:
        nuc_map = np.copy(ann == nuc_id)    
        if not skeleton_norm:
            nuc_dst = distance_transform(nuc_map)
            nuc_dst = 255 * (nuc_dst / np.amax(nuc_dst)) 
            nuc_dst = nuc_dst.astype('uint8')      
        else:
            nuc_dst = distance_transform(nuc_map)
            nuc_dst = 255 * (nuc_dst / np.amax(nuc_dst)) 

            nuc_qtz = np.array(nuc_dst >= 150)
            nuc_ske = skeletonize_3d(nuc_qtz)
            nuc_dst[nuc_ske > 0] = 255

            nuc_dst = nuc_dst.astype('uint8')   

        canvas += nuc_dst
    return canvas
####
def gen_pen_map(ann, mean_nuc=355.4279):
    shape = ann.shape[:2] # HW
    nuc_list = list(np.unique(ann))
    nuc_list.remove(0) # 0 is background

    canvas = np.zeros(shape, dtype=np.float32)

    for nuc_id in nuc_list:
        nuc_map   = np.copy(ann == nuc_id)    
        nuc_size  = nuc_map.sum()
        nuc_ratio = mean_nuc / nuc_size
        canvas[ann == nuc_id] = nuc_ratio
    return canvas
####
def get_nuc_stat(ann, nuc_stat):

    nuc_list = list(np.unique(ann))
    nuc_list.remove(0) # 0 is background

    for nuc_id in nuc_list:
        nuc_map = np.copy(ann == nuc_id)
        nuc_stat.append(nuc_map.sum())
    return nuc_stat 
####
def get_centroid(vertices):
    length = vertices.shape[0]
    sum_x = np.sum(vertices[:, 0])
    sum_y = np.sum(vertices[:, 1])
    cen_x = int(sum_x / length + 0.5)
    cen_y = int(sum_y / length + 0.5)
    return (cen_x, cen_y)
####

imgs_dir = "/home/vqdang/work/data/NUC_Kumar/imgs/"
anns_dir = "/home/vqdang/work/data/NUC_Kumar/msks_fixed/imgs_train/"
save_dir = "/home/vqdang/work/data/NUC_Kumar/anns_proc2/imgs_train/"

# anns_dir = "../../../data/NUC_CPM2017_MICCAI/data/train/anns/"
# save_dir = "../../../data/NUC_CPM2017_MICCAI/data/train/anns_proc/"

file_list = glob.glob(anns_dir + '*.mat')
file_list.sort() # ensure same order [1]

# rm_n_mkdir(save_dir)
for filename in file_list: # png for base
    filename = os.path.basename(filename)
    basename = filename.split('.')[0]
    print(basename)

    # for cpm
    # ann = np.load(anns_dir + basename + '.npy')

    # for kumar
    img = cv2.imread(imgs_dir + basename + '.tif')
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    ann = sio.loadmat(anns_dir + basename + '.mat')
    ann = ann['result']

    # # label and one-hot encode
    # blb = np.array(ann >  0)
    # bgd = np.array(ann == 0)
    # ###
    dst = gen_dst_map(ann, 'euclidean', True) 

    # dst = gen_dst_map(ann, 'euclidean', False)
    # ske = gen_dst_map(ann, 'euclidean', True) 
    # np.save("dst_1.npy", dst)
    # np.save("dst_2.npy", ske)
    # exit()

    # dst = np.load("dst_1.npy")
    # ske = np.load("dst_2.npy") 
    # ax1 = plt.subplot(2,2,1)
    # ax1.imshow(img, cmap=cm.jet)
    # ax2 = plt.subplot(2,2,2, sharex=ax1, sharey=ax1)
    # ax2.imshow(ann)
    # ax3 = plt.subplot(2,2,3, sharex=ax1, sharey=ax1)
    # ax3.imshow(dst)
    # ax3 = plt.subplot(2,2,4, sharex=ax1, sharey=ax1)
    # ax3.imshow(ske)
    # plt.show()  
    
    ann = np.dstack([dst, ann])
    np.save(save_dir + basename + '.npy', ann)

    # dst = np.load("dst_c.npy")
    # dst = dst.astype('float32')
    # dst[ann == 0] = 0

    # mx=np.array([[ 0, 0, 0],
    #              [ 1, 0,-1],
    #              [ 0, 0, 0]])
    # my=np.array([[ 0, 1, 0],
    #              [ 0, 0, 0],
    #              [ 0,-1, 0]])
    # # central difference to get gradient
    # # dont care about the boundary problem
    # cx=ndimage.convolve(dst,mx) / 2
    # cy=ndimage.convolve(dst,my) / 2
    
    # mag = np.sqrt(cx*cx + cy*cy)
    # mag[mag == 0] = 1 # prevent / 0
    # nxi = cx / mag
    # nyi = cy / mag

    # ux = np.ones(nxi.shape)
    # uy = np.zeros(nyi.shape)
    # angle = (nxi * ux + nyi * uy)
    # angle = np.arccos(angle)

    # ax1 = plt.subplot(2,3,1)
    # ax1.imshow(ann, cmap=cm.jet)
    # ax2 = plt.subplot(2,3,2, sharex=ax1, sharey=ax1)
    # ax2.imshow(dst.astype('uint8'))
    # ax3 = plt.subplot(2,3,3, sharex=ax1, sharey=ax1)
    # ax3.imshow(nxi)
    # ax4 = plt.subplot(2,3,4, sharex=ax1, sharey=ax1)
    # ax4.imshow(nyi)
    # ax5 = plt.subplot(2,3,5, sharex=ax1, sharey=ax1)
    # ax5.imshow(angle)
    # ax6 = plt.subplot(2,3,6, sharex=ax1, sharey=ax1)
    # ax6.imshow(uy)
    # plt.show()

#####
# NOTE
# Kumar has lots of nucleus < 20x20, some elongated ones (~100 px width)
# 
