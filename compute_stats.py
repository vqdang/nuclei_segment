
import argparse
import colorsys
import glob
import math
import os
import random
import re  # regex

import cv2
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
from matplotlib import cm
from scipy.ndimage import measurements
from scipy.ndimage.morphology import binary_dilation, distance_transform_edt, binary_erosion
from skimage import morphology as morph
from skimage.feature import peak_local_max

from datagen.utils import randomize_label, rm_n_mkdir
from viz_utils import *

from stats_utils import *

####

random.seed(10) 

print_stats = True

####
valid_same_name = [
    'TCGA-21-5786-01Z-00-DX1',
    'TCGA-49-4488-01Z-00-DX1',
    'TCGA-A7-A13F-01Z-00-DX1',
    'TCGA-B0-5698-01Z-00-DX1',
    'TCGA-B0-5710-01Z-00-DX1',
    'TCGA-CH-5767-01Z-00-DX1',
    'TCGA-E2-A1B5-01Z-00-DX1',
    'TCGA-G9-6336-01Z-00-DX1',]

valid_diff_name = [
    'TCGA-AY-A8YK-01A-01-TS1',
    'TCGA-DK-A2I6-01A-01-TS1',
    'TCGA-G2-A2EK-01A-02-TSB',
    'TCGA-KB-A93J-01A-01-TS1',
    'TCGA-NH-A8F7-01A-01-TS1',
    'TCGA-RD-A8N9-01A-01-TS1',]

imgs_dir = '/mnt/dang/data/KUMAR/train-set/imgs_norm/XXXX/'
true_dir = '/mnt/dang/data/KUMAR/train-set/msks_fixed/imgs_all/'
pred_dir = 'output/v4.0.0.1_valid/proc/'

file_list = glob.glob(imgs_dir + '*.tif')
file_list.sort() # ensure same order [1]

global_inter = 0
global_union = 0
metrics = [[], [], []]
for filename in file_list: # png for base
    filename = os.path.basename(filename)
    basename = filename.split('.')[0]
    
    if basename not in valid_diff_name:
        continue

    img = cv2.imread(imgs_dir + basename + '.tif')
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    
    true = sio.loadmat(true_dir + basename + '.mat')
    true = (true['result']).astype('int32')

    pred = sio.loadmat(pred_dir + basename + '_predicted_map.mat')
    pred = (pred['predicted_map']).astype('int32')

    pred = remap_label(pred)
    true = remap_label(true)
    metrics[0].append(get_dice_1(true, pred))
    metrics[1].append(get_dice_2(true, pred))

    aji_score = get_aji_v2(true, pred, global_inter, global_union)
    global_inter = aji_score[1]
    global_union = aji_score[2]
    metrics[2].append(aji_score[0])
        
    print(basename, end="\t")
    if print_stats:
        for scores in metrics:
            print("%f\t" % scores[-1], end="\t")
    print()
####
metrics = np.array(metrics)
metrics = np.mean(metrics, axis=-1)
metrics = list(metrics)
metrics.append(global_inter/global_union)
print(np.array(metrics))