
import glob
import math
import os
import random
import re  # regex
import shutil
import time

import cv2
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
from matplotlib import cm
from scipy.ndimage.measurements import label as blob_labelling
from scipy.ndimage.morphology import (binary_dilation, binary_erosion,
                                      distance_transform_cdt,
                                      distance_transform_edt)

from datagen.patch_extractor import PatchExtractor
from datagen.utils import *


####
def gen_dst_map(ann, map_type='chessboard'):  
    shape = ann.shape[:2] # HW
    nuc_list = list(np.unique(ann))
    nuc_list.remove(0) # 0 is background

    canvas = np.zeros(shape, dtype=np.uint8)
    distance_transform = distance_transform_cdt \
                        if map_type == 'chessboard' \
                        else distance_transform_edt

    for nuc_id in nuc_list:
        nuc_map = np.copy(ann == nuc_id)    
        nuc_dst = distance_transform(nuc_map)
        nuc_dst = 255 * (nuc_dst / np.amax(nuc_dst)) 
        nuc_dst = nuc_dst.astype('uint8')      
        canvas += nuc_dst
    return canvas

###########################################################################
if __name__ == '__main__':

    extract_type = 'mirror' # 'valid' for fcn8 segnet etc.
                            # 'mirror' for u-net etc.

    step_size = [ 80,  80]
    win_size  = [600, 600]

    xtractor = PatchExtractor(win_size, step_size)

    ####
    valid_same_name = [
        'TCGA-21-5786-01Z-00-DX1',
        'TCGA-49-4488-01Z-00-DX1',
        'TCGA-A7-A13F-01Z-00-DX1',
        'TCGA-B0-5698-01Z-00-DX1',
        'TCGA-B0-5710-01Z-00-DX1',
        'TCGA-CH-5767-01Z-00-DX1',
        'TCGA-E2-A1B5-01Z-00-DX1',
        'TCGA-G9-6336-01Z-00-DX1',]

    valid_diff_name = [
        'TCGA-AY-A8YK-01A-01-TS1',
        'TCGA-DK-A2I6-01A-01-TS1',
        'TCGA-G2-A2EK-01A-02-TSB',
        'TCGA-KB-A93J-01A-01-TS1',
        'TCGA-NH-A8F7-01A-01-TS1',
        'TCGA-RD-A8N9-01A-01-TS1',]

    ####
    data_mode = "train"
    # 5784 is the stain code in SN-SN
    stain_code = "XXXX" # 5784 A1AS A2I6 A13E XXXX
    img_dir = "/mnt/dang/data/KUMAR/train-set/imgs_norm/%s/" % stain_code
    ann_dir = "/mnt/dang/data/KUMAR/train-set/anns_proc/imgs_%s/" % data_mode
    ####
    out_root_path = "/mnt/dang/train/KUMAR/"
    out_dir = "%s/paper/%s/%s/" % (out_root_path, data_mode, stain_code)
    
    file_list = glob.glob(ann_dir + '*.npy')
    file_list.sort() # ensure same order [1]

    patch_list = []
    rm_n_mkdir(out_dir)

    for filename in file_list: # png for base
        filename = os.path.basename(filename)
        basename = filename.split('.')[0]

        img = cv2.imread(img_dir + basename + '.tif')
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB) 

        ann = np.load(ann_dir + basename + '.npy')

        img = np.concatenate([img, ann], axis=-1)
        sub_patches = xtractor.extract(img, extract_type)
        for idx in range(len(sub_patches)):
            patch = sub_patches[idx]
            np.save("{0}/{1}_{2:03d}.npy".format(out_dir, basename, idx), patch)

